<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    public function ed()
    {
        return $this->hasMany(EventDetail::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
    public function coupon()
    {
        return $this->hasMany(Coupon::class);
    }

    public static function getAllEvents($where = [], $filters = [])
    {
    	$query = self::where($where);

    	if (isset($filters['skip'])) {
            $query = $query->skip($filters['skip']);
        }

        if (isset($filters['perPage'])) {
            $query = $query->take($filters['perPage']);
        }

        if (isset($filters['search']) && $filters['search'] != '') {
            $query = $query->where('title', 'LIKE', '%' . $filters['search'] . '%');
        }   

    	return $query->get();
    }
}
