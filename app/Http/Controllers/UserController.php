<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5',
        ]);

        $email = $request->email;
        $password = Hash::make($request->password);
        $user = User::where('email',$email)->first();
        if($user && Hash::check($password, $user->password))
        {
            echo "good";
        }else{
            return redirect()->back()->with('message','Invalid Credentials');
        }

    }
}
