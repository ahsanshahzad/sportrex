<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function showLogin()
    {
        if(Session()->has('isLogin') && Session()->has('admin'))
        {
            return redirect('/dashboard');
        }elseif(Session()->has('isLogin') && Session()->has('user'))
        {
            return redirect('/dashboard');
        }else{
            return view('login');
        }
    }

    public function showRegistration()
    {
        if(Session()->has('isLogin') && Session()->has('admin'))
        {
            return redirect('/dashboard');
        }elseif(Session()->has('isLogin') && Session()->has('user'))
        {
            return redirect('/dashboard');
        }else{
            return view('register');
        }
    }
    
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $user = User::where('email',$request->email)->first();
        if($user && Hash::check($request->password, $user->password))
        {
            if($user->role == 'admin')
            {
                $admin = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ];
                $request->session()->put('admin', $admin);
                session()->put('isLogin', 'login');
                return redirect('/dashboard');
            }else{
                $user = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ];
                $request->session()->put('user', $user);
                session()->put('isLogin', 'login');
                return redirect('/dashboard');
            }
        }
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'source' => 'required',
            'password' => 'required',
        ]);

        $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->role = 'user';
            $user->awarenessfrom = $request->source;
            $user->status = 1;
            $user->password = Hash::make($request->password);
            $user->save();
            
            $user = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ];
                $request->session()->put('user', $user);
                session()->put('isLogin', 'login');
            return redirect('/dashboard');
            
            
    }

    public function logout()
    {
        if(Session()->has('admin'))
        {
            session()->pull('admin');
            
        }else{
            session()->pull('user');
        }
        session()->pull('isLogin');
        return redirect('/login');
        
        
    }
}
