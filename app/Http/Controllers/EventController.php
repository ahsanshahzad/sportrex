<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventDetail;

class EventController extends Controller
{
    public function index()
    {
        $activeEvents = Event::all();
        $upcomingEvents = '';
        $pastEvents = '';
        return view('user.events.events', compact('activeEvents','upcomingEvents','pastEvents'));
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
        ]);
        if($request->file('image')== ''){
            $image = 'page2_img.JPG';
        }else{
        $fileName = $request->file('image')->extension();  
        $fileName = $request->file('image')->getClientOriginalName();
        $request->file('image')->move(public_path('assets/images/events'), $fileName);
        $image = $fileName;
        // echo $fileName;exit;
        }
        $category= $request->category;
        $feesid= $request->fees_id;
        $title=$request->title;
        $venue= $request->venue;
        $venueaddress= $request->venue_address;
        $startdate= $request->start_date;
        $starttime= $request->start_time;
        $enddate= $request->end_date;
        $endtime= $request->end_time;
        $ticketname= $request->ticket_name;
        $ticketquantity= $request->ticket_quantity;
        $ticketprice= $request->ticket_price;
        $ticketdescription= $request->ticket_description;
        $ticketfees= $request->ticket_fees;
        $ticketmax= $request->ticket_max;
        $description= $request->description;
        $eventprivacy= $request->event_privacy;
        $promotiontype= $request->promotion_type;
        $terms= $request->terms;

        $event = new Event;
        $event->category = $category;
        $event->fees_id = $feesid;
        $event->title = $title;
        $event->venue = $venue;
        $event->venue_address = $venueaddress;
        $event->start_date = $startdate;
        $event->start_time = $starttime;
        $event->end_date = $enddate;
        $event->end_time = $endtime;
        $event->image = $image;
        $event->description = $description;
        $event->promotion_type = $promotiontype;
        $event->terms = $terms;
        $event->save();
        if($ticketname != '')
        {
            for($i=0; $i<count($ticketname); $i++)
            {
                $ed = new EventDetail;
                $ed->event_id = $event->id;
                $ed->ticket_name = $ticketname[$i];
                $ed->ticket_quantity = $ticketquantity[$i];
                $ed->ticket_price = $ticketprice[$i];
                $ed->ticket_description = $ticketdescription[$i];
                $ed->ticket_max = $ticketmax[$i];
                $ed->ticket_fees = $ticketfees[$i];
                $ed->save();
            }
        }
        return redirect()->back()->with('message','Event created successfuly');
        
    }
    
    public function sale()
    {
        return view('user.sales.sales_reports');
    }
    
    public function ticket()
    {
        return view('user.tickets.tickets');
    }

    public function destroy($id)
    {
        dd($id);
        Event::destroy($id);
        return redirect()->back();
    }  
}
