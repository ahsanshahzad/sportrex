<?php

namespace App\Http\Controllers;
use Mail;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Event;
use App\Models\EventDetail;
use App\Models\PurchaserDetail;
use App\Models\TicketOwnerDetail;
use Neonexxa\BillplzWrapperV3\BillplzBill;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $categories = Category::all();
        $upcomingEvents = Event::orderBy('start_date','desc')->get();
        return view('index', compact('categories','upcomingEvents'));
    }

    public function cat_view(Request $request, $id)
    {
        $totalEvents = Event::getAllEvents(['category_id' => $id])->count();
        $perPage     = 10;
        $currentPage = $request->page > 0 ? $request->page : 1; // default always 1
        $skip        = ($currentPage * $perPage) - $perPage;
        $total_pages = ceil($totalEvents / $perPage);
        $events = Event::getAllEvents(['category_id' => $id], ['skip' => $skip, 'perPage' => $perPage]);
          
        $category = Category::find($id);
        return view('events', compact('events','category','total_pages','currentPage','totalEvents'));
    }

    public function event_detail($id)
    {
        $event = Event::find($id);
        
        return view('event_detail', compact('event'));
    }

    public function booking($id)
    {
        $event_detail = EventDetail::find($id);
        return view('booking', compact('event_detail'));
    }

    public function booking_info(Request $request)
    {
        // dd($request->all());
        
    
        $request->validate([
            'p_name' => 'required',
            'p_email' => 'required|email',
            'p_phone' => 'required'
        
        ],
        [
            'p_name.required' => 'Name is required',
            'p_email.required' => 'Email is required',
            'p_phone.required' => 'Phone is required'
        ]);

        $data = new PurchaserDetail;
        $data->name = $request->p_name;
        $data->email = $request->p_email;
        $data->phone = $request->p_phone;
        $data->event_id = $request->id;
       
        if($request->coupon != ''){
            $data->coupon = $request->coupon;
        }
        $data->save();
        $o_name = $request->o_name;
        // echo '<pre>';
        // print_r($o_name);
        $o_email = $request->o_email;
        $o_phone = $request->o_phone;
        if($o_name != '' && $o_email != '' && $o_phone != '' )
        {
            for($i=0; $i<count($o_name); $i++)
            {
                $data2 = new TicketOwnerDetail;
                $data2->purchaser_id  = $data->id;
                $data2->name  = $o_name[$i];
                $data2->email  = $o_email[$i];
                $data2->phone  = $o_phone[$i];
                $data2->save();
            }
            
        }
        
        return redirect('/checkout/'.$request->id);
    }

    public function checkout($id)
    {

        $ed = EventDetail::find($id);
        return view('checkout', compact('ed'));
    }

    public function checkout_info(Request $request)
    {
        //dd($request->all());
        //
        // $params = $request->all();
        // $product = Purchase::find($params['product']);
        // from the guide
        $res0 = new BillplzBill;
        $res0->collection_id ='S-LtXbMK2jeaoHDa0TzJR1Ag'; 
        $res0->description = "New BIll"; 
        $res0->email = 'ahsanshahzad920@gmail.com'; 
        $res0->name = 'ahsan'; 
        $res0->amount = 2*100; 
        $res0->callback_url = "http://127.0.0.1:222/"; 
        // and other optional params
        $res0 = $res0->create_bill();
        //dd($res0);
        list($rhead ,$rbody, $rurl) = explode("\r\n", $res0);
        $bplz_result = json_decode($rurl);
        dd($bplz_result);
        // Store the bill into our purchases
        $purchase = new Purchase;
        $purchase->user_id = Auth::user()->id;
        $purchase->product_id = $product->id;
        $purchase->bill_id = $bplz_result->id;
        $purchase->save();
        return redirect($bplz_result->url);
    }

    public function catSearchEvents(Request $request)
    {
        
        $title = $request->title;
        $address = $request->address;
        $time = $request->time;
        $fe = Event::where('title','like', '%'.$title.'%')
                    ->where('venue_address','like', '%'.$address.'%')
                    ->where('category_id', $request->cat_id)->first();
        
        if($fe && substr($fe->start_time,0,2) == substr($time,0,2))
        {
            return view('filter_category_events', compact('fe'));
        }else{
            $fe = '';
            return view('filter_category_events', compact('fe'));
        }
        
    }

    public function findEvent()
    {
        $events = Event::paginate(12);
        return view('find_event', compact('events'));
    }
    
    public function indexEventSearch(Request $request)
    {

        $filters['search']  = (isset($request->search) && $request->search != '') ? $request->search : '';
        $filters['perPage']     = 10;
        $currentPage = $request->page > 0 ? $request->page : 1; // default always 1
        $filters['skip']        = ($currentPage * $filters['perPage'] ) - $filters['perPage'] ;
        $totalEvents = Event::getAllEvents([], ['search' => $filters['search']])->count();
        $total_pages = ceil($totalEvents / $filters['perPage'] );
        $events = Event::getAllEvents([], $filters);
        $query = $filters['search'];
                           
        $categories = Category::all();
        $upcomingEvents = Event::orderBy('start_date','desc')->get();
        return view('index', compact('categories','events','total_pages','currentPage', 'query','totalEvents','upcomingEvents'));
    }

    public function allSearchEvents(Request $request)
    {
        $title = $request->title;
        $address = $request->address;
        $time = $request->time;
        $fe = Event::where('title','like', '%'.$title.'%')
                    ->where('venue_address','like', '%'.$address.'%')->first();
        if($fe && substr($fe->start_time,0,2) == substr($time,0,2))
        {
            return view('filter_all_events', compact('fe'));
        }else{
            $fe = '';
            return view('filter_all_events', compact('fe'));
        }
        
    }

    public function contacts(Request $request)
    {
         $data = $request->except([
        '_token',
        '_method',
        ]);
                 
        if ($data['email'] != '') {
            Mail::send( 
                'emails.contactus',
                [
                    'data' => $data
                ],
                function ($message) use ($data) {
                    $message->to(config('app.contact_email'), config('app.contact_email'));
                    if (config('mail.bcc.address') != '') {
                        $message->bcc(config('mail.bcc.address'), config('mail.bcc.name'));
                    }
                    $message->replyTo(config('mail.from.address'), config('mail.from.name'));
                    $message->subject('Contact Us @Sportrex');
                }
            );
        }

        return redirect()
            ->route('contact')
            ->with('success', 'Mail has been sent succuessfully.');
    }
    public function test(){
        return view('orgnizer_details');
        
    }
}
