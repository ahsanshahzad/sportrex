<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Coupon;
use App\Models\Event;

class CouponController extends Controller
{
    public function index()
    {
        $coupons = Coupon::with(['event'])->get();
       // dd($coupons);
        $events = Event::all();
        return view('user.coupons.coupons', compact('coupons','events'));
    }
    
    public function store(Request $request)
    {//dd($request->all());
        $request->validate([
                'event_id' => 'required',
                'title' => 'required',
                'code' => 'required'
            ]);
            
        $data = new Coupon;
        $data->event_id = $request->event_id;
        $data->title = $request->title;
        $data->code = $request->code;
        $data->discount_type = $request->discount_type;
        $data->discount_amount = $request->discount_amount;
        $data->max_usage = $request->max_usage;
        $data->save();
        if($data)
        {
            return redirect()->back()->with('message','Coupon Has been inserted');
        }else{
            return redirect()->back()->with('err_message','Some problem occure please try again');
        }
    }

    public function destroy($id)
    {
        //dd($id);
        Coupon::destroy($id);
        return redirect('coupons');
    }   
}