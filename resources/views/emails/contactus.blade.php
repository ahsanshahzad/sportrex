<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    <div style="background-color: #f4f4f4; padding:10px; margin:auto; font-family: Arial, Helvetica, sans-serif; line-height:30px; ">
            <img src="{!! asset('assets\images\logo\logo1.png') !!}" alt="" width="180" style="margin:20px auto;"/>
        <div style="background: #ffffff; margin:10px; padding:10px;">
           <p><b>Hello Admin,</b></p>
<p>{!! $data['name'] !!} has contacted via the contact form. Please check the form details below:</p>

<p>
    <b>Name:</b> {!! $data['name'] !!}
    <br />
    <b>Email:</b> {!! $data['email'] !!}
    <br />
    <b>Message:</b> {!! nl2br($data['message']) !!}
</p>

<p>Thanks,</p>

<p>
    <b>Best Regards,</b>
    <br />
    {!! $data['name'] !!}
</p>

        </div>
        <div style="text-align:center; font-size:12px;">
            <a style="color:#373736; text-decoration: none;" href="{!! route('contact') !!}">Contact us</a>
            <a style="color:#373736; margin:0 15px;text-decoration: none;"  href="#">Terms & Conditions</a>
            <a style="color:#373736;text-decoration: none;"  href="#">Privacy Policy</a>
            <p style="margin:0px;">© {!! date('Y') !!} SPORTREX All rights reserved.</p></div>
    </div>
</body>
</html>