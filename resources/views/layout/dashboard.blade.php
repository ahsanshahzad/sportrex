<style type="text/css">
	.filter-toggle .date-btn {
		padding: 0.3rem 0.75rem;
		border-radius: 15px;
	}
</style>
<!doctype html>
	<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
		
		
		
		<title>Sports</title>
		<!-- <title></title> -->
		<meta name="description" content="Sports">
		
		<meta name="author" content="Sports">
		
		<meta name="robots" content="noindex, nofollow">
		
		
		<meta name="csrf-token" content="OMALtmJhCZyFaVGwpqwCw9DwvNVVfuhyhL02ymbk">
		
		
		<link rel="shortcut icon" href="{{asset('assets/media/favicons/favicon.png')}}">
		
		<link rel="icon" sizes="192x192" type="image/png" href="{{asset('assets/media/favicons/favicon-192x192.png')}}">
		
		<link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/media/favicons/apple-touch-icon-180x180.png')}}">
		
		
		
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
		
		<link rel="stylesheet" id="css-main" href="{{asset('assets/css/oneui.css')}}">
		<link rel="stylesheet" id="css-preloader" href="{{asset('assets/css/pre-loader.css')}}">
		<link rel="stylesheet" href="{{asset('assets/css/themes/flat.css')}}">
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" >
		
		
		<style>
			.buttons-html5,.buttons-print{
				color: #fff;
				background-color: #44b3ab;
				border-color: #44b3ab;
			}
			
			@media (min-width: 768px) {
				.pull-right{
					bottom: 20px;
					position: relative;
					padding: 10px;
					float:right;
				}
				.right_align .pull-right{
					bottom:0;
					padding: 5px;
				}
				.breadcrumb{
					padding: 0px!important;
				}
				.dataTables_paginate {
					bottom: 40px;
					position: relative;
					float: right;
				}
				.dataTables_paginate .page-link{
					font-weight:normal;
				}
			}
		</style>
		
		<link rel="stylesheet" href="{{asset('assets/bundles/datatables/datatables.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
		<style>
			#myDataTable{
				width:100%;
			}
			.dataTables_wrapper > .btn-group, .dataTables_wrapper > .btn-group-vertical {
				position: relative;
				display: inline-flex;
				vertical-align: middle;
				float: right;
			}
			
			.btn-sm{
				margin-left:35%;
			}
			
		</style>
	</head>
	<body>
		
		<!-- <div id="preloader">
				<div id="ctn-preloader" class="ctn-preloader">
						<div class="animation-preloader">
								<div class="spinner"></div>
								<div class="txt-loading">
										<span data-text-preloader="I" class="letters-loading">
										I                        </span>
										<span data-text-preloader="N" class="letters-loading">
										N                        </span>
										<span data-text-preloader="V" class="letters-loading">
										V                        </span>
										<span data-text-preloader="E" class="letters-loading">
										E                        </span>
										<span data-text-preloader="N" class="letters-loading">
										N                        </span>
										<span data-text-preloader="T" class="letters-loading">
										T                        </span>
								</div>
								<p class="text-center"> Loading </p>
						</div>
						<div class="loader">
								<div class="row">
										<div class="col-lg-3 loader-section section-left"><div class="bg"></div></div>
										<div class="col-lg-3 loader-section section-left"><div class="bg"></div></div>
										<div class="col-lg-3 loader-section section-right"><div class="bg"></div></div>
										<div class="col-lg-3 loader-section section-right"><div class="bg"></div></div>
								</div>
						</div>
				</div>
			</div> -->


			<div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-fixed main-content-narrow page-header-dark sidebar-dark">


				@yield('content')


			</div>
			<!-- OneUI Core JS -->

			<script src="{{asset('assets//js/oneui.app.js')}}"></script>
			<script src="{{asset('assets/jquery-tabledit/jquery.tabledit.min.js')}}"></script>
			<script src="{{asset('assets/table-edits/build/table-edits.min.js')}}"></script>
			<!-- <script src="http://sports.voagstech.com/assets/table-edits/table-editable.init.js"></script> -->
			<script src="{{asset('assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

			<script src="{{asset('assets/chart.js/Chart.min.js')}}"></script>
			<script src="{{asset('assets/chart.js/charts-custom.js')}}"></script>
			<script type='text/javascript' id='saasland-custom-wp-js-after'>

		//<![CDATA[
		jQuery(window).on( 'load', function() { // makes sure the whole site is loaded
		jQuery( '#status' ).fadeOut(); // will first fade out the loading animation
		jQuery( '#preloader' ).delay(250).fadeOut( 'slow' ); // will fade out the white DIV that covers the website.
		jQuery( 'body' ).delay(250).css({'overflow':'visible'});
	})
		//]]>

		;(function($){
			$(document).ready(function () {
				$('.widget_search').removeClass('widget_search').addClass('search_widget_two');
			});
		})(jQuery);
	</script>
	<script>
		jQuery(document).ready(function(e) {



			var oTable = $('#myDataTable').DataTable({

				processing: true,

				serverSide: true,

				stateSave: true,

				searching: false,
				
				Filter: true,

				dom : 'Blfrtip',

				autoWidth: false,

				buttons: [
				'copy', 'excel', 'pdf'
				],

				ajax: {

					url: "http://sports.voagstech.com/dashboard/datatable",

					data: function (d) {

						d.event = $('#s_event').val();
						d.user = $('#s_user').val();
						d.created_at = $('#s_date').val();

					}

				}, columns: [

				{data: 'DT_RowIndex', name: 'DT_RowIndex',"searchable": false,"orderable":false},
				{data: 'event', name: 'event'},
				{data: 'user', name: 'user'},
				{data: 'booked_quantity', name: 'booked_quantity'},
				{data: 'total', name: 'total'},
				{data: 'created_at', name: 'created_at'}
				/*"searchable": false*/
				]

			});

		/* $('#data-search-form').on('submit', function (e) {
		
		oTable.draw();

		e.preventDefault();
		
	});*/

	$('#s_event').on('keyup', function (e) {

		oTable.draw();

		e.preventDefault();

	});
	$('#s_user').on('keyup', function (e) {

		oTable.draw();

		e.preventDefault();

	});
		/*  $('#s_date').on('change', function (e) {
		
		oTable.draw();
		
		e.preventDefault();
		
	});*/




});

</script>
<script src="{{asset('assets/bundles/datatables/datatables.min.js')}}"></script>
<script src="{{asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script>

	function fetchdata(){

		$.ajax({
			type: 'GET',
			url: 'http://sports.voagstech.com/orders/checkuserarrived',
			success: function (data) {
				One.helpers('notify', {type: 'success',icon:'fa fa-coin',align: 'center', message: 'Record Deleted Successfully'});
				if(!(data))
				{
					$('#notification').css("display","none");
					$('#dothdn').css("display","none");
					$('#notificationnone').css("display","inline");
				}
				else
				{
					$('#notificationnone').css("display","none");
					$('#notification').html(data).css("display","inline");
					$('#dothdn').css("display","inline");
				}
			}
		});
	}

	$(document).ready(function(){
		/* fetchdata();
		setInterval(fetchdata,5000);*/
		jQuery(function(){One.helpers(['table-tools-checkable', 'table-tools-sections']);});
	});

</script>
@yield('scripts')
</body>
</html>