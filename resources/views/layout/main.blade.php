<html>
<head>
    <title>Sportrex</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
<link rel="stylesheet" href="{{asset('assets/css/carousel.css')}}">

<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">


@yield('links')
<style type="text/css">
.text-black{
color: black;
}
.no-border{
    border: 0px;
    
}
#home-title {
    padding-top: 0px;
}
.bg-color{
    background-color: #f8f8f8;
}
</style>
</head>
<body>
   

@yield('content')
        
    

    
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}" ></script>
<script src="{{asset('assets/js/carousel.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script>

//Slider 1

$(document).ready(function()
{


if($('.bbb_viewed_slider').length)
{
var viewedSlider = $('.bbb_viewed_slider');

viewedSlider.owlCarousel(
{
loop:true,
margin:0,
autoplay:true,
autoplayTimeout:5000,
nav:false,
dots:false,
responsive:
{
0:{items:1.5},
575:{items:2},
768:{items:3},
991:{items:4},
1199:{items:6}
}
});

if($('.bbb_viewed_prev').length)
{
var prev = $('.bbb_viewed_prev');
prev.on('click', function()
{
viewedSlider.trigger('prev.owl.carousel');
});
}

if($('.bbb_viewed_next').length)
{
var next = $('.bbb_viewed_next');
next.on('click', function()
{
viewedSlider.trigger('next.owl.carousel');
});
}
}


});

//Slider 2

$(document).ready(function()
{


if($('.bbb_viewed_slider_1').length)
{
var viewedSlider = $('.bbb_viewed_slider_1');

viewedSlider.owlCarousel(
{
loop:true,
margin:0,
autoplay:true,
autoplayTimeout:5000,
nav:false,
dots:false,
responsive:
{
0:{items:1},
575:{items:2},
768:{items:3},
}
});

if($('.bbb_viewed_prev_1').length)
{
var prev = $('.bbb_viewed_prev_1');
prev.on('click', function()
{
viewedSlider.trigger('prev.owl.carousel');
});
}

if($('.bbb_viewed_next_1').length)
{
var next = $('.bbb_viewed_next_1');
next.on('click', function()
{
viewedSlider.trigger('next.owl.carousel');
});}}

});

//Slider 3


$(document).ready(function()
{


if($('.bbb_viewed_slider_2').length)
{
var viewedSlider = $('.bbb_viewed_slider_2');

viewedSlider.owlCarousel(
{
loop:true,
margin:0,
autoplay:true,
autoplayTimeout:5000,
nav:false,
dots:false,
responsive:
{
0:{items:1},
575:{items:2},
768:{items:3},

}
});

if($('.bbb_viewed_prev_2').length)
{
var prev = $('.bbb_viewed_prev_2');
prev.on('click', function()
{
viewedSlider.trigger('prev.owl.carousel');
});
}

if($('.bbb_viewed_next_2').length)
{
var next = $('.bbb_viewed_next_2');
next.on('click', function()
{
viewedSlider.trigger('next.owl.carousel');
});}}

});


</script>

</html>