@extends('layout.main')
@section('links')
  
    
@endsection
@section('content')
@include('includes.menu')
<!--owl Slider 2-->
@if(isset($events))
<div class="container">
<div class="col-lg-12 col-md-12 margin-top-10">
    <h4> Total Events Found {!! $totalEvents !!}</h4>
</div>
<div class="row">
<!-- Listing Item -->

@forelse($events as $event)
<div class="col-lg-4 col-md-6 margin-top-10">
    <a href="{{url('event/detail/'.$event->id)}}" class="listing-item-container">
        <div class="listing-item">
            <!-- <div class="listing-badge now-open" style="background: #FF5A19">From: ₦55,000</div> -->
            <img src="{{asset('public/assets/images/events/'.$event->image)}}" class="img-fluid" alt="Public Speaking Coaching">
        </div>
        <div class="star-rating" style="height: 170px;">
            <span><i class="fa fa-map-marker"></i> {{$event->venue_address}}</span><br>
            <h4><strong>{{$event->title}}</strong></h4>
            <span><i class="fa fa-calendar-check-o"></i> Saturday, January 08</span>
            <span class="button popup-with-zoom-anim share-this" title="Share with friends" href="#small-dialog" data-url="https://tickethub.ng/public-speaking-coaching-39072998" data-title="Public Speaking Coaching"><i class="like-icon im im-icon-Sharethis"></i></span>
        </div>
    </a>
</div>
@empty
<h3>No Record Found</h3>
@endforelse
</div>

    <div class="col-md-12">
        <center>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="{!! $currentPage > 1 ? route('index.search') . '?query=' . $query . '&page=' . ($currentPage-1) : 'javascript:;' !!}">Previous</a>
                    </li>

                    @for ($p = 1; $p <= $total_pages; $p++)
                        <li class="page-item {!! $p == $currentPage ? 'active' : '' !!}">
                            <a class="page-link" href="{!! $p == $currentPage ? 'javascript:;' : '?query=' . $query . '&page=' . $p !!}">
                                {!! $p !!}
                            </a>
                        </li>
                    @endfor

                    <li class="page-item">
                        <a class="page-link" href="{!! $currentPage < $total_pages ? route('index.search') . '?query=' . $query . '&page='. ($currentPage+1) : 'javascript:;' !!}">Next</a>
                    </li>
                </ul>
            </nav>
        </center>
    </div>
</div>
@endif
<!--owl Slider 1-->
<div class="sec2">
    <div class="row">
        <div class="col-md-12">
            <div class="bbb_main_container">
                <div class="bbb_viewed_title_container">
                    <h3 class="bbb_viewed_title">CATEGORIES</h3>
                    <div class="bbb_viewed_nav_container">
                        <div class="bbb_viewed_nav bbb_viewed_prev"><img class = "small_icon" src = "{{asset('public/assets/images/prev.png')}}"></div>
                        <div class="bbb_viewed_nav bbb_viewed_next"><img class = "small_icon" src = "{{asset('public/assets/images/next.png')}}"></div>
                    </div>
                </div>
                <div class="bbb_viewed_slider_container">
                    <div class="owl-carousel owl-theme bbb_viewed_slider">
                        @forelse($categories as $cat)
                        <div class="owl-item">
                            <div class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                <div class="bbb_viewed_image">
                                    <a href="{{url('category/view/'.$cat->id)}}">
                                        <img src="{{asset('public/assets/images/'.$cat->image)}}" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        @empty
                        <div class="owl-item">
                            <div class="bbb_viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                <div class="bbb_viewed_image">No Category Found</div>
                                
                                
                            </div>
                        </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--owl Slider 2-->
<div class="sec3" id="findeventsection">
    <div class="row">
        <div class="col-md-12">
            <div class="bbb_main_container">
                <div class="bbb_viewed_title_container">
                    <h2 class="bbb_viewed_title">UPCOMING EVENTS</h2>
                    <h4 class = "slider2_title">Check out these incredible sport events</h4>
                    <div class="bbb_viewed_nav_container">
                        <div class="bbb_viewed_nav bbb_viewed_prev_1"><img class = "small_icon" src = "{{asset('public/assets/images/prev.png')}}"></div>
                        <div class="bbb_viewed_nav bbb_viewed_next_1"><img class = "small_icon" src = "{{asset('public/assets/images/next.png')}}"></div>
                    </div>
                </div>
                <div class="bbb_viewed_slider_container">
                    <div class="owl-carousel owl-theme bbb_viewed_slider_1">
                        
                        @forelse($upcomingEvents as $event)
                         <a href="{{URL::to('/event/detail').'/'.$event->id}}">
                        <div class="owl-item">
                            <div class="bbb_viewed_item discount d-flex flex-column justify-content-center">
                                <div class="bbb_viewed_image_1"><img src="{{asset('public/assets/images/events/'.$event->image)}}" alt=""></div>
                                <h5 class = "slider_bottom_heading" >{{$event->title}}</h5>
                                <h6 class = "slider_bottom_heading_2">{{$event->start_date}} {{$event->start_time}}</h6>
                                <p class = "slider_bottom_paragraph">{{$event->venue_address}}</p>
                                
                            </div>
                        </div>
                        </a>
                        @empty
                            <h3>No Record Found</h3>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--owl Slider 3-->
<div class="sec4">
    <div class="row">
        <div class="col-md-12">
            <div class="bbb_main_container">
                <div class="bbb_viewed_title_container">
                    <h3 class="bbb_viewed_title">POPULAR EVENTS</h3>
                    <div class="bbb_viewed_nav_container">
                        <div class="bbb_viewed_nav bbb_viewed_prev_2"><img class = "small_icon" src = "{{asset('public/assets/images/prev.png')}}"></div>
                        <div class="bbb_viewed_nav bbb_viewed_next_2"><img class = "small_icon" src = "{{asset('public/assets/images/next.png')}}"></div>
                    </div>
                </div>
                <div class="bbb_viewed_slider_container">
                    <div class="owl-carousel owl-theme bbb_viewed_slider_2">
                    <a href="#">
                    <div class="owl-item">
                
                            <div class="bbb_viewed_item discount d-flex flex-column justify-content-center">
                                <div class="bbb_viewed_image_2"><img src="{{asset('public/assets/images/Capture.JPG')}}" alt=""></div>
                                <h5 class = "slider_bottom_heading">Football Tournament 2021</h5>
                                <h6 class = "slider_bottom_heading_2">25 September 2021, 8:00 AM</h6>
                                <p class = "slider_bottom_paragraph">Block A, 36545, Jalan TS 6/1, Taman Perindustrian Subang,
                                47510 Subang Jaya, Selangor</p>
                                
                                
                            </div>

                        </div>
                        
                        </a>
                        <a href="#">
                        <div class="owl-item">
                            <div class="bbb_viewed_item discount d-flex flex-column justify-content-center ">
                                <div class="bbb_viewed_image_2"><img src="{{asset('public/assets/images/Capture.JPG')}}" alt=""></div>
                                <h5 class = "slider_bottom_heading">Milo Game Wanted Football</h5>
                                <h6 class = "slider_bottom_heading_2">25 September 2021, 8:00 AM</h6>
                                <p class = "slider_bottom_paragraph">Block A, 36545, Jalan TS 6/1, Taman Perindustrian Subang,
                                47510 Subang Jaya, Selangor</p>
                                
                                
                            </div>
                        </div>  </a>
                        <a href="#">
                        <div class="owl-item">
                            <div class="bbb_viewed_item d-flex flex-column justify-content-center ">
                                <div class="bbb_viewed_image_2"><img src="{{asset('public/assets/images/Capture.JPG')}}" alt=""></div>
                                <h5 class = "slider_bottom_heading">Milo Game Wanted Football</h5>
                                <h6 class = "slider_bottom_heading_2">25 September 2021, 8:00 AM</h6>
                                <p class = "slider_bottom_paragraph">Block A, 36545, Jalan TS 6/1, Taman Perindustrian Subang,
                                47510 Subang Jaya, Selangor</p>
                                
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sec 4 slider-->
<div class="sec4">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <p class = "by_location">BY LOCATION</p>
            <h1 class = "sec4_paragraph">BROWSE
            POPULAR
            CITIES</h1>
            <h2 class = "sec4_heading">Browse listings in popular places</h2>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="gallery">
                <div class="gallery-container">
                    <img class="gallery-item gallery-item-1" src="{{asset('public/assets/images/slider_2.JPG')}}" data-index="1">
                    <img class="gallery-item gallery-item-2" src="{{asset('public/assets/images/slider_2.JPG')}}" data-index="2">
                    <img class="gallery-item gallery-item-3" src="{{asset('public/assets/images/slider_2.JPG')}}" data-index="3">
                    <img class="gallery-item gallery-item-4" src="{{asset('public/assets/images/slider_2.JPG')}}" data-index="4">
                    <img class="gallery-item gallery-item-5" src="{{asset('public/assets/images/slider_2.JPG')}}" data-index="5">
                </div>
                <div class="gallery-controls">
                    
                    <a class = "gallery-controls-next"><img class = "arrow_left" src="{{asset('public/assets/images/arrow_left.png')}}" alt=""> </a>
                    <a class = "gallery-controls-previous"><img class = "arrow_right gallery-controls-next" src="{{asset('public/assets/images/arrow_right.png')}}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--sec 5 slider-->
<div class="sec5_bg page1sec5">
<div class="sec5">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class = "sec5_heading">BECOME AN ORGANIZER</h1>
            <p class="sec5_paragraph">Earn extra income and unlock new</p>
            <p class="sec5_paragraph_1">opportunities by hosting great sport event</p>
            <a href=""class = "join_button">JOIN NOW</a>
            
        </div></div>
    </div>
</div>
<!--sec 6 slider-->
@include('includes.footer')
@endsection