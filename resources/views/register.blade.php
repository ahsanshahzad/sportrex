<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Sportrex</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
		<link rel="stylesheet" href="{{asset('public/assets/css/carousel.css')}}">
		<link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
		<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
		<link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('public/assets/css/log.css')}}">
	</head>
	<body>
		<div class="sec_1 page2">
			@include('includes.simple_menu')
			
		
		</div>
		<div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100">
					<form class="login100-form validate-form" autocomplete="off" action="{{ route('register') }}" method="post">
						@csrf
						<span class="login100-form-title p-b-26">
							Welcome
						</span>
						<span class="login100-form-title p-b-48">
							<i class="zmdi zmdi-font"></i>
						</span>
						<div class="wrap-input100 validate-input"  >
							<input class="input100" type="text" value="{{old('name')}}" required="" name="name">
							<span class="focus-input100" data-placeholder="Full Name"></span>
						</div>
						@error('name')
						<span class="alert alert-danger" role="alert">
							{{ $message }}
						</span>
						@enderror
						<div class="wrap-input100 validate-input"  >
							<input class="input100" type="text" value="{{old('phone')}}" name="phone">
							<span class="focus-input100" data-placeholder="Mobile Number"></span>
						</div>
						@error('phone')
						<span class="alert alert-danger" role="alert">
							{{ $message }}
						</span>
						@enderror
						<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
							<input class="input100" type="text" name="email" value="{{old('email')}}">
							<span class="focus-input100" data-placeholder="Email"></span>
						</div>
						@error('email')
						<span class="alert alert-danger" role="alert">
							{{ $message }}
						</span>
						@enderror
						<div class="wrap-input100 validate-input" data-validate="Enter password" >
							<span class="btn-show-pass">
								<i class="zmdi zmdi-eye"></i>
							</span>
							<input class="input100" type="password" name="password">
							<span class="focus-input100" data-placeholder="Password"></span>
						</div>
						@error('password')
						<div class="alert alert-danger" role="alert">
							{{ $message }}
						</div>
						@enderror
						<label for="">How did you hear about us?
						</label>
						<select data-placeholder="Select Item" class="chosen-select input100 form-group " style="border-radius: 10px;" name="source"  required="">
							
							<option value="">-- Select an option --</option>
							<option value="Google search">Google search</option>
							<option value="Facebook">Facebook</option>
							<option value="Twitter">Twitter</option>
							<option value="Friends">Friends</option>
							<option value="Posters & Handbills">Posters & Handbills</option>
							<option value="Others">Others</option>
							
						</select>
						@error('source')
						<span class="alert alert-danger" role="alert">
							{{ $message }}
						</span>
						@enderror
						
						<div class="container-login100-form-btn">
							<div class="wrap-login100-form-btn">
								<div class="login100-form-bgbtn"></div>
								<button class="login100-form-btn btnSignUp" style="background-color:#40324A">
								Sign Up
								</button>
							</div>
						</div>
						
						<div class="text-center p-t-115">
							<span class="txt1">
								Already have an account?
							</span>
							
							<a class="txt2" href="{{route('login')}}">
								Sign In
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div id="dropDownSelect1"></div>
		<!--sec 6 slider-->
		@include('includes.footer')
			</body>
		</html>