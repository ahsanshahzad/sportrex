@extends('layout.main')
@section('content')
<style type="text/css">
.text-black{
color: black;
}
.no-border{
    border: 0px;
    
}
#home-title {
    padding-top: 0px;
}
.bg-color{
    background-color: #f8f8f8;
}
</style>
<link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
<link rel="stylesheet" href="https://tickethub.ng/assets/front/css/style.css">
@include('includes.top_navbar')
<div class="container">
   
<div class="row">
<!-- Listing Item -->
@if($fe)
<div class="col-lg-4 col-md-6 margin-top-10">
    <a href="{{url('event/detail/'.$fe->id)}}" class="listing-item-container">
        <div class="listing-item">
            <!-- <div class="listing-badge now-open" style="background: #FF5A19">From: ₦55,000</div> -->
            <img src="{{asset('public/assets/images/events/'.$fe->image)}}" class="img-fluid" alt="Public Speaking Coaching">
        </div>
        <div class="star-rating" style="height: 170px;">
            <span><i class="fa fa-map-marker"></i> {{$fe->venue_address}}</span><br>
            <h4><strong>{{$fe->title}}</strong></h4>
            <span><i class="fa fa-calendar-check-o"></i> Saturday, January 08</span>
            <span class="button popup-with-zoom-anim share-this" title="Share with friends" href="#small-dialog" data-url="https://tickethub.ng/public-speaking-coaching-39072998" data-title="Public Speaking Coaching"><i class="like-icon im im-icon-Sharethis"></i></span>
        </div>
    </a>
</div>
@else
<h3>No Record Found</h3>
@endif

<!-- Listing Item / End -->

</div>
</div>
@include('includes.footer')
@endsection