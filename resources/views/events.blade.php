@extends('layout.main')
@section('links')
    
    <link rel="stylesheet" href="https://tickethub.ng/assets/front/css/style.css">
@endsection
@section('content')
@include('includes.top_navbar')
 <!--owl Slider 2-->
 <div class="sec3">
    <div class="row">
        <div class="col-md-12">
            <div class="bbb_main_container">
                <div class="bbb_viewed_title_container">
                    <h2 class="bbb_viewed_title">UPCOMING EVENTS {!! $totalEvents !!}</h2>
                    <h4 class = "slider2_title">Check out these incredible sport events</h4>
                    <div class="bbb_viewed_nav_container">
                        <div class="bbb_viewed_nav bbb_viewed_prev_1"><img class = "small_icon" src = "{{asset('public/assets/images/prev.png')}}"></div>
                        <div class="bbb_viewed_nav bbb_viewed_next_1"><img class = "small_icon" src = "{{asset('public/assets/images/next.png')}}"></div>
                    </div>
                </div>
                <div class="bbb_viewed_slider_container">
                    <div class="owl-carousel owl-theme bbb_viewed_slider_1">
                        @forelse($events as $event)
                        <a href="{{URL::to('/event/detail').'/'.$event->id}}">
                        <div class="owl-item">
                            <div class="bbb_viewed_item discount d-flex flex-column justify-content-center">
                                <div class="bbb_viewed_image_1"><img src="{{asset('public/assets/images/events/'.$event->image)}}" alt=""></div>
                                <h5 class = "slider_bottom_heading" >{{$event->title}}</h5>
                                <h6 class = "slider_bottom_heading_2">25 September 2021, 8:00 AM</h6>
                                <p class = "slider_bottom_paragraph">{{$event->venue_address}}</p>
                                
                            </div>
                        </div>
                        </a>
                        @empty
                            <h3>No Record Found</h3>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="container">-->
<!--<div class="col-lg-12 col-md-12 margin-top-10">-->
<!--    <h4> Total Events Found {!! $totalEvents !!}</h4>-->
<!--</div>-->
<!--<div class="row">-->
<!-- Listing Item -->
<!--@forelse($events as $event)-->
<!--<div class="col-lg-4 col-md-6 margin-top-10">-->
<!--    <a href="{{url('event/detail/'.$event->id)}}" class="listing-item-container">-->
<!--        <div class="listing-item">-->
            <!-- <div class="listing-badge now-open" style="background: #FF5A19">From: ₦55,000</div> -->
<!--            <img src="{{asset('public/assets/images/events/'.$event->image)}}" alt="Public Speaking Coaching">-->
<!--        </div>-->
<!--        <div class="star-rating" style="height: 170px;">-->
<!--            <span><i class="fa fa-map-marker"></i> {{$event->venue_address}}</span><br>-->
<!--            <h4><strong>{{$event->title}}</strong></h4>-->
<!--            <span><i class="fa fa-calendar-check-o"></i> Saturday, January 08</span>-->
<!--            <span class="button popup-with-zoom-anim share-this" title="Share with friends" href="#small-dialog" data-url="https://tickethub.ng/public-speaking-coaching-39072998" data-title="Public Speaking Coaching"><i class="like-icon im im-icon-Sharethis"></i></span>-->
<!--        </div>-->
<!--    </a>-->
<!--</div>-->
<!--@empty-->
<!--<h3>No Record Found</h3>-->
<!--@endforelse-->
<!-- Listing Item / End -->
<!--<p></p>-->
<!--    <div class="col-md-12">-->
<!--        <center>-->
<!--            <nav aria-label="Page navigation example">-->
<!--                <ul class="pagination">-->
<!--                    <li class="page-item">-->
<!--                        <a class="page-link" href="{!! $currentPage > 1 ? route('event-category', $category->id) . '?page=' . ($currentPage-1) : 'javascript:;' !!}">Previous</a>-->
<!--                    </li>-->

<!--                    @for ($p = 1; $p <= $total_pages; $p++)-->
<!--                        <li class="page-item {!! $p == $currentPage ? 'active' : '' !!}">-->
<!--                            <a class="page-link" href="{!! $p == $currentPage ? 'javascript:;' : route('event-category', $category->id) . '?page=' . $p !!}">-->
<!--                                {!! $p !!}-->
<!--                            </a>-->
<!--                        </li>-->
<!--                    @endfor-->

<!--                    <li class="page-item">-->
<!--                        <a class="page-link" href="{!! $currentPage < $total_pages ? route('event-category', $category->id) . '?page=' . ($currentPage+1) : 'javascript:;' !!}">Next</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </nav>-->
<!--        </center>-->
<!--    </div>-->
<!--</div>-->

<!--<p></p>-->
<!--<div class="mb-3" style="float:right;"> -->

<!--</div>-->
<!--</div>-->
@include('includes.footer')
@endsection