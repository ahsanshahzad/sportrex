@extends('layout.main')
@section('content')
<link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
<style type="text/css">
    .bg-color{
        background-color: #ff5900;
    }
</style>
@include('includes.simple_menu')
</div>
<div class="sec2 img_sec">
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        @if($event)
        @if($event->image != '')
        <img class="img-fluid" src="{{asset('public/assets/images/events/'.$event->image)}}" alt="">
        @else
        <img class="img-fluid" src="{{asset('public/assets/images/page2_img.JPG')}}" alt="">
        @endif
        @endif
        
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="card">
            <h3 class="card_heading">How to start an E-commerce
            Business</h3>
            <h5 class="card_sub_heading">Saturday, 25 September 2021, 8:00 AM</h5>
            <p class="card_paragraph">Block A, 36545, Jalan TS 6/1, Taman
                Perindustrian Subang, 47510 Subang Jaya,
            Selangor</p>
            <hr></hr>
            <h3 class="card_heading">Booking Summary</h3>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <h4 class="summary_heading">3x Normal</h4>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <h4 class="summary_heading">RM 120</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <h4 class="summary_heading">1x Advance</h4>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <h4 class="summary_heading">RM 250</h4>
                </div>
            </div>
            <hr></hr>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <h4 class="card_heading">Total</h4>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <h4 class="card_heading_1">RM 350</h4>
                </div>
            </div>
            <a class="register_button bg-color" href="{{url('/booking/'.'10')}}">Book Now</a>
        </div>
        
    </div>
    
</div>

</div>
<!--sec 3 Tabs-->
<div class="sec3">
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <!-- Tabs -->
        <div class="container">
            <div class="row">
                <div class="">
                    <!-- Nav tabs -->
                    <div id="cards1" style="margin-top: 60px;" class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#home" class="active" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Get Ticket</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Location</a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Organizer</a></li>
                            
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <legend>Description</legend>
                                <p>{{$event->description}}</p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <!-- <legend>Get Ticket</legend>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. -->
                            </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                    <legend>Location</legend>
                                    {{$event->venue_address}}
                                </div>
                                    <div role="tabpanel" class="tab-pane" id="settings">
                                        <legend>Settings</legend>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passage..</div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./Tabs -->
                </div>
            </div>
        </div>
        <!--sec 4 Get ticket-->
        <div class="sec4">
            
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h2 class="get_ticker_haeding">Get Ticket</h2>
                    <hr>
                    <!--Advance Section-->
                    @if($event)
                    @forelse($event->ed as $ed)
                    
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <h3 class="counter_heading col-6">Ticket Name : </h3>
                                <p class="counter_heading_2">{{$ed->ticket_name}}</p>
                            </div>
                            <div class="row">
                                <h3 class="counter_heading col-6">Ticket Price : </h3>
                                <p class="counter_heading_2">${{$ed->ticket_price}}</p>
                            </div>
                            <div class="row">
                                <h3 class="counter_heading col-6">Ticket Quantity : </h3>
                                <p class="counter_heading_2">{{$ed->ticket_quantity}}</p>
                            </div>
                        </div>
                        
                        <div id="ticket-col1" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a class="btn btn-btn-primary" data-id="{{$ed->id}}" onclick="minus2(this)"><img class="counter_img" src="{{asset('public/assets/images/minus.png')}}"></a>
                            <input type="text" id="count2{{$ed->id}}" placeholder="0" class="counter" value="0">
                            <a class="btn btn-btn-primary" data-id="{{$ed->id}}" onclick="plus2(this)"><img class="counter_img" src="{{asset('public/assets/images/plus.png')}}"></a>
                            
                        </div>
                    </div><br>
                    <div class="row d-none">
                        <div class="col-8"></div>
                        <div class="col-4">
                            <a class="btn bg-color" href="{{url('/booking/'.$ed->id)}}">Book Now</a>
                        </div>
                    </div>
                    <hr></hr>
                    @empty
                    <h3>No Ticket Found</h3>
                    @endforelse
                    @endif
                    <!--Maps-->
                    
                    <h2 class="loaction_haeding">Location</h2>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13297.573034012228!2d73.12315824999999!3d33.5691373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38dfed29437d4c61%3A0x8e8f61cff0c89df0!2sCheezious!5e0!3m2!1sen!2s!4v1641747112663!5m2!1sen!2s" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    <!--Follow Section-->
                    <div class="row organize_margin">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <h3 class="organize_heading">Organize by</h3>
                            <h3 class="organize_heading_2"><a href="/orgnizer-details">Sportizza Subang Sdn Bhd</a></h3>
                            
                        </div>
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 button_margin">
                            <a class = "follow_button" href="">Follow</a>
                            
                            
                        </div>
                    </div>
                    <!--Share Button -->
                    <div class="row share_margin">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            
                            <a class = "share_button" href=""><i class="fas fa-paper-plane"></i> Share</a>
                            
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--sec 5 map-->
        <div class="sec5">
            <div class="row">
                <div class="col-md-12">
                    <div class="bbb_main_container">
                        <div class="bbb_viewed_title_container">
                            <h3 class="bbb_viewed_title">Also on <span class="bbb_viewed_title_span">SPORTREX</span>  </h3>
                            <div class="bbb_viewed_nav_container">
                            </div>
                        </div>
                        <div class="bbb_viewed_slider_container">
                            <div class="owl-carousel owl-theme bbb_viewed_slider_2">
                             
                            <div class="owl-item">
                                   <a href="#">
                            
                            <div class="bbb_viewed_item discount d-flex flex-column justify-content-center">
                                        <div class="bbb_viewed_image_2 position-relative"><img src="{{asset('public/assets/images/Capture.JPG')}}" alt="">
                                    
                                       <a href="#" class="position-absolute sendBtn"> <img src="{{asset('public/assets/images/sendbtn.svg')}}" alt=""></a>
                                    </div>
                                        <div class="slider_card ">
                                            <h5 class = "slider_bottom_heading_1">How to start an E-commerce <br>
                                            Business</h5>
                                            <h6 class = "slider_bottom_heading_2">25 September 2021, 8:00 AM</h6>
                                            <p class = "slider_bottom_paragraph">Block A, 36545, Jalan TS 6/1, Taman Perindustrian Subang,
                                            47510 Subang Jaya, Selangor</p>
                                        </div>
                                        
                                        
                                    </div>
                                    </a>
                                </div>
                                
                                
                                
                                <div class="owl-item">
                                <a href="#">
                                    <div class="bbb_viewed_item discount d-flex flex-column justify-content-center ">
                                        <div class="bbb_viewed_image_2 position-relative"><img src="{{asset('public/assets/images/Capture.JPG')}}" alt="">
                                    
                                        <a href="#" class="position-absolute sendBtn"> <img src="{{asset('public/assets/images/sendbtn.svg')}}" alt=""></a>
                                    </div>
                                        <div class="slider_card">
                                            <h5 class = "slider_bottom_heading">How to grow budget</h5>
                                            <h6 class = "slider_bottom_heading_2">25 September 2021, 8:00 AM</h6>
                                            <p class = "slider_bottom_paragraph">Block A, 36545, Jalan TS 6/1, Taman Perindustrian Subang,
                                            47510 Subang Jaya, Selangor</p>
                                        </div>
                                        
                                    </div>
                                    </a>
                                </div>
                                <div class="owl-item">
                                <a href="#">
                                    <div class="bbb_viewed_item d-flex flex-column justify-content-center ">
                                        <div class="bbb_viewed_image_2 position-relative"><img src="{{asset('public/assets/images/Capture.JPG')}}" alt="">
                                    
                                        <a href="#" class="position-absolute sendBtn"> <img src="{{asset('public/assets/images/sendbtn.svg')}}" alt=""></a>
                                    </div>
                                        <div class="slider_card">
                                            <h5 class = "slider_bottom_heading">How to be confident</h5>
                                            <h6 class = "slider_bottom_heading_2">25 September 2021, 8:00 AM</h6>
                                            <p class = "slider_bottom_paragraph">Block A, 36545, Jalan TS 6/1, Taman Perindustrian Subang,
                                            47510 Subang Jaya, Selangor</p>
                                        </div>
                                        
                                    </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br><br><br><br>
    
    
    @include('includes.footer')
    <!--counter 1-->
    <script>
    var count = 0;
    var countEl = document.getElementById("count");
    function plus() {
    count++;
    countEl.value = count;
    }
    function minus() {
    count--;
    countEl.value = count;
    }
    var count2 = 0;
    var countE2 = document.getElementById("count2");
    function plus2(evt) {
        id=$(evt).attr('data-id');
        total=parseInt($('#count2'+id).val());
     
        final=parseInt(total) + 1;
        $("#count2"+id).val(final);

    // countE2.value = count2;
    }
    function minus2(evt) {
        id=$(evt).attr('data-id');
        total=parseInt($('#count2'+id).val());
        if(total!==0){
        final=parseInt(total) - 1;
        $("#count2"+id).val(final);
         }
    // count2--;
    // countE2.value = count2;
    }
    </script>
    <!--counter 2-->
    <script>
    //Slider 1
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider').length)
    {
    var viewedSlider = $('.bbb_viewed_slider');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    991:{items:4},
    1199:{items:6}
    }
    });
    if($('.bbb_viewed_prev').length)
    {
    var prev = $('.bbb_viewed_prev');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next').length)
    {
    var next = $('.bbb_viewed_next');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });
    }
    }
    });
    //Slider 2
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider_1').length)
    {
    var viewedSlider = $('.bbb_viewed_slider_1');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    }
    });
    if($('.bbb_viewed_prev_1').length)
    {
    var prev = $('.bbb_viewed_prev_1');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next_1').length)
    {
    var next = $('.bbb_viewed_next_1');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });}}
    });
    //Slider 3
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider_2').length)
    {
    var viewedSlider = $('.bbb_viewed_slider_2');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    }
    });
    if($('.bbb_viewed_prev_2').length)
    {
    var prev = $('.bbb_viewed_prev_2');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next_2').length)
    {
    var next = $('.bbb_viewed_next_2');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });}}
    });
    </script>
    <script>
    let map;
    function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 8,
    });
    }
    
    
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg&callback=initMap&v=weekly&channel=2" async></script>
    @endsection