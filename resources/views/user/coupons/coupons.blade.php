@extends('layout.dashboard')
@section('content')
<!-- Sidebar -->
@include('includes.dashboard.sidebar')
<!-- navbar -->
@include('includes.dashboard.navbar')
<main id="main-container">
<!-- Hero -->
<div class="bg-body-light">
    <div class="content">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill h3 my-2">
            Coupon
            </h1>
        </div>
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <nav class="flex-sm-00-auto" aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-alt">
                    <li class="breadcrumb-item">
                        <a class="link-fx" href="https://voagstech.com/sports/dashboard">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Coupons
                    </li>
                </ol>
            </nav>
            
            <a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-primary btn-add-new pull-right">
                <i class="fa fa-plus-square fa-lg"></i> Add New
            </a>
        </div>
        @if(Session::has('message'))
            <p class="alert alert-info">{{ Session::get('message') }}</p>
        @endif
    </div>
</div>
<!-- END Hero -->
<!-- Page Content -->
<div class="content">
    <div class="block block-rounded block-themed">
        <div class="block-content">
            
            <form method="post" role="form" id="data-search-form">
                <div class="table-responsive">
                    
                    <table class="table table-striped table-hover"  id="myDataTable">
                        <thead>
                            <tr role="row" class="heading">
                                <td>
                                    <input type="text" class="form-control" id="s_title" autocomplete="off" placeholder="Title">
                                </td>
                                
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr role="row" class="heading">
                                <th>Title</th>
                                <th>Event</th>
                                <th>Code</th>
                                <th>Discount Type</th>
                                <th>Discount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($coupons as $event)
                                <tr>
                                    <td>{{@$event->title}}</td>
                                    <td>{{@$event->event->title}}</td>
                                    <td>{{@$event->code}}</td>
                                    <td>{{@$event->discount_type}}</td>
                                    <td>{{@$event->discount_amount}}</td>
                                    <td>
                                                <div class="btn-group" role="group" aria-label="Horizontal Info">
                                                    <!-- <a class="btn btn-outline-primary" href="#" title="View Details">
                                            			<i class="fa fa-edit"></i>
                                            		</a>  -->
                                                    <a class="btn btn-outline-primary" href="javascript:;" onclick="confirmDelete('{{$event->id}}')">
                                            			<i class="fa fa-trash fa-lg text-danger btnRemove"></i>
                                            		</a>
                                                    
                                                            <form id="delete-blog-{{$event->id}}"
                                                                  action="{{ route('coupons.destroy', $event->id) }}"
                                                                  method="POST" style="display: none;">
                                                                @method('DELETE')
                                                                @csrf
                                                            </form>
                                                </div>
                                            </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END Page Content -->

<div class="modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
    <div class="block block-rounded block-themed mb-0">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add New</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option btn_close_modal" data-bs-dismiss="modal" aria-label="Close">
                <i class="fa fa-fw fa-times"></i>
                </button>
            </div>
        </div>
        
        <form method="POST" action="{{route('coupons.store')}}">
        @csrf
        <div class="block-content fs-sm">
            <div class="row justify-content-center mt-2 mb-2 form-group">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="event">Event</label>
                        </div>
                        <div class="col-sm-8">
                            <select class="form-control" name="event_id">
                                <option selected="selected" value="">select event</option>
                                @if($events)
                                    @foreach($events as $event)
                                        <option value="{{$event->id}}">{{$event->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-4">
                            <label for="title">Title</label>
                        </div>
                        <div class="col-sm-8">
                            <input class="form-control" name="title" type="text" id="title">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-4">
                            <label for="code">Coupen Code</label>
                        </div>
                        <div class="col-sm-8">
                            <input class="form-control" name="code" type="text" id="code">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-4">
                            <label for="discount_amount">Discount Type</label>
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-1">
                                    &nbsp;
                                </div>
                                <div class="col-sm-4 custom-control custom-radio custom-control-primary">
                                    <input type="radio" value="fixed" class="custom-control-input " id="fixed" name="discount_type">
                                    <label class="custom-control-label" for="fixed"><strong>Fixed</strong></label>
                                </div>
                                <div class="col-sm-5 custom-control custom-radio custom-control-primary">
                                    <input type="radio" value="percentege" class="custom-control-input " id="percentege" name="discount_type">
                                    <label class="custom-control-label" for="percentege"><strong>Percentege</strong></label>
                                </div>
                            </div>
                        </div>
                    </div><div class="row mt-3">
                    <div class="col-sm-4">
                        <label for="discount_amount">Discount Value</label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" name="discount_amount" type="text" id="discount_amount">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <label for="max_usage">Set Limit:</label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" name="max_usage" type="number" id="max_usage">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-8">
                        <input class="btn btn-primary" type="submit" value="Save">
                        <a href="" class="btn btn-outline-dark">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
                        </div>
                        
                    </form>
                    
                </div>
            </div>
</div>
</div>

</main>
    <!-- Footer -->

        <footer id="page-footer" class="bg-body-light">

            <div class="content py-3">

                <div class="row font-size-sm">

                    <!-- <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">

                        Crafted with <i class="fa fa-heart text-danger"></i>

                    </div> -->

                    <div class="  col-sm-12 order-sm-1 py-1 text-center  ">

                        &copy; <span data-toggle="year-copy"></span> <a class="font-w600" href="https://voagstech.com/sports/dashboard" target="_blank">Sports</a>

                    </div>

                </div>

            </div>

        </footer>

        <!-- END Footer -->
    @endsection
    @section('scripts')

    <script>
    jQuery(document).ready(function(e) {
        if(jQuery('.btn_close_modal'))
    	{
    		jQuery('.btn_close_modal').click(function(e) {
                $('#createModal').modal('hide');
            });
    	}
    });
    </script>
    <script>
        function confirmDelete(id) {
            let choice = confirm('Are you sure, You want to delete ?');
            if (choice) {
                document.getElementById('delete-blog-' + id).submit();
            }
        }
    </script>
    @endsection