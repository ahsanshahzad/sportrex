@extends('layout.dashboard')
@section('content')
<aside id="side-overlay" class="font-size-sm">
	
	<div class="content-header border-bottom">
		
		<a class="img-link mr-1" href="javascript:void(0)">
			
			<img class="img-avatar img-avatar32" src="http://sports.voagstech.com/assets//media/avatars/avatar10.jpg" alt="">
			
		</a>
		
		<div class="ml-2">
			
			<a class="text-dark font-w600 font-size-sm" href="javascript:void(0)">Manager Sport Complex</a>
			
		</div>
		
		<a class="ml-auto btn btn-sm btn-alt-danger" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_close">
			
			<i class="fa fa-times"></i>
			
		</a>
		
	</div>
	
	<div class="content-side">
		
		<h6>Profile Details</h6>
		
		<ul class="list-group mb-5">
			
			<li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-user-tie"></i></span><span class="bg-light rounded px-2">Manager Sport Complex</span></li>
			
			<li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-briefcase"></i></span><span class="bg-light rounded px-2"></span></li>
			
			<li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-phone"></i></span><span class="bg-light rounded px-2">+923068672409</span></li>
			
			<li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-envelope"></i></span><span class="bg-light rounded px-2">admin@gmail.com</span></li>
			
		</ul>
		
		
		
		<h6>Change Password</h6>
		
		<form class="bg-light rounded px-4 py-2" action="http://sports.voagstech.com/users/updatePassword" method="POST">
			
			<input type="hidden" name="_token" value="OMALtmJhCZyFaVGwpqwCw9DwvNVVfuhyhL02ymbk">
			<div class="my-2">
				
				<label for="currentPass">Current Password</label>
				
				<input type="text" class="form-control" name="current_password" id="currentPass">
				
			</div>
			
			
			
			<div class="my-2">
				
				<label for="newPass">New Password</label>
				
				<input type="text" class="form-control" name="new_password" id="newPass">
				
			</div>
			
			
			
			<div class="mt-2 mb-3">
				
				<input type="submit" value="Change Password" class="btn btn-dark btn-block">
				
			</div>
			
		</form>
		
	</div>
	
</aside>
@include('includes.dashboard.sidebar')
@include('includes.dashboard.navbar')

<main id="main-container">


<!-- Hero -->
<!--     <div class="bg-body-light">
	<div class="content content-full">
		<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
			<h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
			<nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
				<ol class="breadcrumb breadcrumb-alt">
					<li class="breadcrumb-item">App</li>
					<li class="breadcrumb-item">
						<a class="link-fx" href="">Dashboard
						</li>
					</ol>
				</nav>
			</div>
		</div>
	</div> -->
	<!-- END Hero -->
	
	<!-- Hero -->
	<div class="bg-body-light">
		<div class="content">
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<h1 class="flex-sm-fill h3 my-2">
				Dashboard
				</h1>
				
				<!--   <div class="filter-toggle btn-group">
					<button class="btn btn-primary date-btn active" data-start_date="2021-12-29" data-end_date="2021-12-29">Today</button>
					<button class="btn btn-primary date-btn "data-start_date="2021-12-22" data-end_date="2021-12-29">Last 7 Days</button>
					<button class="btn btn-primary date-btn" data-start_date="2021-12-01" data-end_date="2021-12-29">This Month</button>
					<button class="btn btn-primary date-btn" data-start_date="2021-01-01" data-end_date="2021-12-31">This Year</button>
				</div> -->
			</div>
			
			<div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
				<nav class="flex-sm-00-auto" aria-label="breadcrumb">
					<ol class="breadcrumb breadcrumb-alt">
						<li class="breadcrumb-item">
							<a class="link-fx" href="http://sports.voagstech.com/dashboard">Home</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">
							Dashboard
						</li>
					</ol>
				</nav>
				
			</div>
		</div>
	</div>
	<!-- END Hero -->
	
	
	<!-- Page Content -->
	<div class="content">
		<div class="row row-deck">
			<div class="col-sm-6 col-xl-3">
				<div class="block block-rounded d-flex flex-column">
					<div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
						<dl class="mb-0">
							<dt class="font-size-h2 font-w700">16</dt>
							<dd class="text-muted mb-0">Total Event(s)</dd>
						</dl>
						<div class="item item-rounded bg-body">
							<i class="fa fa-calendar  font-size-h3 text-primary"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xl-3">
				<div class="block block-rounded d-flex flex-column">
					<div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
						<dl class="mb-0">
							<dt class="font-size-h2 font-w700">28</dt>
							<dd class="text-muted mb-0">Total Ticket Sold</dd>
						</dl>
						<div class="item item-rounded bg-body">
							<i class="fa fa-tag font-size-h3 text-warning"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xl-3">
				<div class="block block-rounded d-flex flex-column">
					<div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
						<dl class="mb-0">
							<dt class="font-size-h2 font-w700">12156</dt>
							<dd class="text-muted mb-0">Total Cash Out</dd>
						</dl>
						<div class="item item-rounded bg-body">
							<i class="fa fa-coins font-size-h3 text-info"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xl-3">
				<div class="block block-rounded d-flex flex-column">
					<div class="block-content block-content-full flex-grow-1 d-flex justify-content-between align-items-center">
						<dl class="mb-0">
							<dt class="font-size-h2 font-w700">0</dt>
							<dd class="text-muted mb-0">Current Balance</dd>
						</dl>
						<div class="item item-rounded bg-body">
							<i class="fa fa-check-circle font-size-h3 text-success"></i>
							<!-- fa-check-circle -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-12 col-xl-12">
				<div class="block">
					<div class="block-content">
						<h3 class="block-title mb-3">Recent Ticket Sold</h3>
						
						
						<div class="table-responsive">
							
							<table class="table table-striped table-hover"  id="myDataTable">
								
								<thead>
									<tr role="row" class="heading" colspan="4">
										<td>&nbsp;</td>
										<td>
											<input type="text" class="form-control" id="s_event" autocomplete="off" placeholder="Search Event">
										</td>
										<td>
											<input type="text" class="form-control" id="s_user" autocomplete="off" placeholder="Search User">
										</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<!-- <td>
											<input type="date" class="form-control" id="s_date" autocomplete="off" >
										</td> -->
										<tr>
											<th>#</th>
											<!-- <th>Store</th> -->
											<th>Event Name</th>
											<th>User</th>
											<th>Booked QTY</th>
											<th>Amount Earned</th>
											
											<th>Date Purchased</th>
											<!--   <th>Status</th>
											<th>Action</th> -->
										</tr>
									</thead>
								<tbody>                            </tbody>
							</table>
						</div>
						
						<a href="javascript:void(0)" class="mb-3 btn btn-primary">View All</a>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
		
		
		<!-- END Chart.js Charts -->
		
		
		
	</div>
	
	
	
	<!-- END Page Content -->
</main>

<!-- Footer -->

<footer id="page-footer" class="bg-body-light">
	
	<div class="content py-3">
		
		<div class="row font-size-sm">
			
			<!-- <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
				
				Crafted with <i class="fa fa-heart text-danger"></i>
				
			</div> -->
			
			<div class="  col-sm-12 order-sm-1 py-1 text-center  ">
				
				&copy; <span data-toggle="year-copy"></span> <a class="font-w600" href="http://sports.voagstech.com/dashboard" target="_blank">Sports</a>
				
			</div>
			
		</div>
		
	</div>
	
</footer>

<!-- END Footer -->





<!-- Apps Modal -->


<div class="modal fade" id="one-modal-apps" tabindex="-1" role="dialog" aria-labelledby="one-modal-apps" aria-hidden="true">
	
	<div class="modal-dialog modal-sm" role="document">
		
		<div class="modal-content">
			
			<div class="block block-rounded block-themed block-transparent mb-0">
				
				<div class="block-header bg-primary-dark">
					
					<h3 class="block-title">Apps</h3>
					
					<div class="block-options">
						
						<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
						
						<i class="si si-close"></i>
						
						</button>
						
					</div>
					
				</div>
				
				<div class="block-content block-content-full">
					
					<div class="row gutters-tiny">
						
						<div class="col-6">
							
							<!-- CRM -->
							
							<a class="block block-rounded block-link-shadow bg-body" href="javascript:void(0)">
								
								<div class="block-content text-center">
									
									<i class="si si-speedometer fa-2x text-primary"></i>
									
									<p class="font-w600 font-size-sm mt-2 mb-3">
										
										CRM
										
									</p>
									
								</div>
								
							</a>
							
							<!-- END CRM -->
							
						</div>
						
						<div class="col-6">
							
							<!-- Items -->
							
							<a class="block block-rounded block-link-shadow bg-body" href="items">
								
								<div class="block-content text-center">
									
									<i class="si si-rocket fa-2x text-primary"></i>
									
									<p class="font-w600 font-size-sm mt-2 mb-3">
										
										Items
										
									</p>
									
								</div>
								
							</a>
							
							<!-- END Items -->
							
						</div>
						
						<div class="col-6">
							
							<!-- Sales -->
							
							<a class="block block-rounded block-link-shadow bg-body mb-0" href="javascript:void(0)">
								
								<div class="block-content text-center">
									
									<i class="si si-plane fa-2x text-primary"></i>
									
									<p class="font-w600 font-size-sm mt-2 mb-3">
										
										Sales
										
									</p>
									
								</div>
								
							</a>
							
							<!-- END Sales -->
							
						</div>
						
						<div class="col-6">
							
							<!-- Payments -->
							
							<a class="block block-rounded block-link-shadow bg-body mb-0" href="javascript:void(0)">
								
								<div class="block-content text-center">
									
									<i class="si si-wallet fa-2x text-primary"></i>
									
									<p class="font-w600 font-size-sm mt-2 mb-3">
										
										Payments
										
									</p>
									
								</div>
								
							</a>
							
							<!-- END Payments -->
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</div>

<!-- END Apps Modal -->
@endsection