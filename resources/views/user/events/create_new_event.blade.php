@extends('layout.dashboard')
@section('content')

<!-- Sidebar -->
@include('includes.dashboard.sidebar')
<!-- navbar -->
@include('includes.dashboard.navbar')
<main id="main-container">
    
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                Create New Event
                </h1>
            </div>
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <nav class="flex-sm-00-auto" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a class="link-fx" href="http://sports.voagstech.com/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a class="link-fx" href="http://sports.voagstech.com/events">Events</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            Create New
                        </li>
                    </ol>
                </nav>
                
                <a href="{{route('events')}}" class="btn btn-dark btn-return pull-right">
                    <i class="fa fa-chevron-left mr-2"></i> Return to Listing
                </a>
            </div>
        </div>
    </div>
    <!-- END Hero -->
    
    
    <!-- Page Content -->
    <div class="content">
        
        @if(session()->has('message'))
        <div class="alert alert-success">{{session()->get('message')}}</div>
        @endif
        <!-- Validation Wizards -->
        <!-- <h2 class="content-heading">Validation Wizards</h2> -->
        
        <!-- Form -->
        <form method="POST" action="{{route('create.event')}}" accept-charset="UTF-8" class="js-wizard-validation-form" enctype="multipart/form-data">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <!-- Steps Content -->
            <div class="block   block-rounded block-themed">
                <div class="block-header">
                    <h3 class="block-title"><span class="form-wizard-count">1</span> Event info <br> <small>Tell us a bit about your event</small></h3>
                    <div class="block-options">
                        <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                    </div>
                </div>
                <div class="block-content">
                    <!-- Step 1 -->
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-validation-firstname">Title</label>
                                <input class="form-control" value="{{old('title')}}"  placeholder="Event Title" type="text" id="title" name="title">
                            </div>
                            @error('title')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-validation-lastname">Vaneu</label>
                                <input class="form-control"  placeholder="Event Venue" type="text" id="wizard-validation-lastname" name="venue" value="{{old('venue')}}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label for="venue_address">Vaneu Address</label>
                        <input class="form-control" type="text" id="venue_address" value="{{old('venue_address')}}"  placeholder="Venue Address" name="venue_address" >
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="start_date">Start Date</label>
                                
                                <input class="form-control" value="{{old('start_date')}}"  type="date" id="start_date" name="start_date">
                                
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="start_date">Start Time</label>
                                
                                
                                <input class="form-control" value="{{old('start_time')}}"  type="time" id="start_time" name="start_time">
                                
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="start_date">End Date</label>
                                
                                <input class="form-control" value="{{old('end_date')}}"   type="date" id="end_date" name="end_date">
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="start_date">End Time</label>
                                
                                
                                <input class="form-control" value="{{old('end_time')}}"   type="time" id="end_time" name="end_time">
                                
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label for="image">Image  (Note: Image should be (3579 x 2551 px))</label>
                        <input type="file" name="image">
                    </div>
                    
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description" cols="50" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <!-- END Step 1 -->
            
            <!-- Step 2 -->
            <div class="block   block-rounded block-themed ">
                <div class="block-header">
                    <h3 class="block-title"><span class="form-wizard-count">2</span> Create Ticket <br><small>Create ticket type(s) for this event</small></h3>
                    <div class="block-options">
                        <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row ">
                        <div class="form-group col-sm-4">
                            <label for="ticket_name">Ticket Name</label>
                            
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="ticket_quantity">Ticket QTY</label>
                            
                            
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="ticket_price">Ticket Price</label>
                            
                        </div>
                        <div class=" form-group col-sm-1">
                            <label for="action">Action</label>
                            
                            
                        </div>
                    </div>
                    
                    <div class="row variation_div mb-0" id="variation_div">&nbsp;</div>
                    <!-- append here -->
                    
                    
                    <div class="row pl-5  pb-4 " >
                        <div class="mt-4"></div>
                        <div class="col-md-10 text-center ">
                            Choose a ticket type to start with. Default (Free - Unlimited)
                            <div class="btn-group btn-group-justified" style="width:100%">
                                
                                <a   class="btn btn-primary btn-rounded ticket_add" data-ticket-type="free"><i class="icon-add position-left visible-md visible-lg"></i> FREE <span class="hidden-xs">TICKET</span></a>
                                
                                <a class="btn btn-info btn-rounded ticket_add" data-ticket-type="paid"><i class="icon-add position-left visible-md visible-lg"></i> PAID <span class="hidden-xs">TICKET</span></a>
                                
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
            <!-- END Step 2 -->
            
            <!-- Step 3 -->
            <div class="block   block-rounded block-themed mt-4">
                <div class="block-header">
                    <h3 class="block-title"><span class="form-wizard-count">3</span> Additional Information <br><small>Tell us a little bit more about your event?</small></h3>
                    <div class="block-options">
                        <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox custom-control-primary">
                            <input type="checkbox" class="custom-control-input" id="remaining_ticket" name="remaining_ticket"  >
                            <label class="custom-control-label" for="remaining_ticket"><strong>Show Remaining Tickets</strong>: Show the number of remaining tickets on your events.</label>
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <div class="custom-control custom-checkbox custom-control-primary">
                            <input type="checkbox" class="custom-control-input" id="org_details" name="org_details" >
                            <label class="custom-control-label" for="org_details"><strong>Show Organizer Details</strong>: Show the details of the organizer on your events.</label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="custom-control custom-checkbox custom-control-primary">
                            <input type="checkbox" class="custom-control-input" id="can_comment" name="can_comment" >
                            <label class="custom-control-label" for="can_comment"><strong>Activate Comments</strong>:Allow users to comment on your event.</label>
                        </div>
                    </div>
                    
                    <h3> Event Listing Privacy</h3>
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-primary">
                            <input type="radio" value="public" class="custom-control-input event_type" id="public_event" name="event_privacy"
                            checked>
                            <label class="custom-control-label" for="public_event"><strong>Public Event</strong>: Your event can be found by anyone on our site, our promotion partners, and search engines.</label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-primary">
                            <input type="radio" value="private" class="custom-control-input event_type" id="private_event" name="event_privacy"   >
                            <label class="custom-control-label" for="private_event"><strong>Private Event</strong>: Only accessible by people you specify.</label>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group  " id="category_type">
                                
                                <label for="category">Event Category</label>
                                <select class="form-control" id="category" name="category"><option selected="selected" value="">select</option><option value="1">Badminton</option><option value="6">Bicycle</option><option value="5">Esports</option><option value="3">Football</option><option value="2">Golf</option><option value="4">Running</option></select>
                                <!--    <small><i>Please specify if transaction fees would be passed on to buyers or charged from your account. We charge </i></small> -->
                            </div>
                            <div class="form-group   d-none " id="link_type" >
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-radio custom-control-primary">
                                            <input type="radio" value="link" class="custom-control-input " id="link_promotion" name="promotion_type" checked>
                                            <label class="custom-control-label" for="link_promotion"><strong>Link Invite</strong>: Anyone who has your event link.</label>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <div class="custom-control custom-radio custom-control-primary">
                                            <input type="radio" value="email" class="custom-control-input " id="email_promotion" name="promotion_type" >
                                            <label class="custom-control-label" for="email_promotion"><strong>Email Invite</strong>: Only people you send a unique invitation .</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-validation-skills">Fees Setting</label>
                                <select class="form-control" id="wizard-validation-skills" name="fees_id">
                                    <option value="">Please select Fees Setting</option>
                                    <option value="1"  >Absorb Fees</option>
                                    <option value="2" >Pass On</option>
                                    
                                </select>
                                <small><i>Please specify if transaction fees would be passed on to buyers or charged from your account. We charge </i></small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox custom-control-primary">
                            <input type="checkbox" class="custom-control-input" id="wizard-validation-terms" name="terms" value="accepted">
                            <label class="custom-control-label" for="wizard-validation-terms">Agree with the terms <a href="javascript:void('0')"><i>terms</i></a></label>
                        </div>
                    </div>
                    <div class="row ">
                        
                        <div class="col-12 text-right mb-3">
                            
                            <button type="submit" class="btn btn-primary  " data-wizard="finish">
                            <i class="fa fa-check mr-1"></i> Submit
                            </button>
                        </div>
                    </div>
                    
                    <!-- END Step 3 -->
                </div>
                <!-- END Steps Content -->
                
                <!-- Steps Navigation -->
                
                
                
                <!-- END Steps Navigation -->
                
                
                <!-- END Validation Wizard Classic -->
            </div>
            
            <!-- END Form -->
        </div>
    </form>
    
    
    <!-- END Validation Wizards -->
    
    
    
    
    
</div>
<!-- clone paid dive start here -->
<div class="clone_paid d-none"  style=" margin-top: 5px;   padding: 10px; ">
    <div class="row ">
        <div class="form-group col-sm-4">
            
            <input placeholder="Name [ English ]" class="form-control" name="ticket_name[]" type="text">
        </div>
        <div class="form-group col-sm-3">
            
            <input placeholder="Total Quantity" class="form-control" name="ticket_quantity[]" type="text">
            
        </div>
        <div class="form-group col-sm-3">
            
            <input placeholder="Ticket Price" class="form-control" name="ticket_price[]" type="text">
        </div>
        <div class=" form-group col-sm-1">
            
            <div class=" row ">
                <div class="col-md-4">
                    <a href="javascript:void(0)"><i class="fa fa-cog fa-lg text-primary btnSetting"></i></a>
                </div> <div class="col-md-4">
                <a href="javascript:void(0)"><i class="fa fa-trash fa-lg text-danger btnRemove"></i></a>
            </div>
            
        </div>
        
    </div>
</div>
<!-- PAID TICKET on setting -->
<div class="row d-none bg-light  other_div"  style="width:90%;">
    <input type="hidden" value="paid" name="">
    <div class="form-group col-sm-6 col-lg-6">
        <label for="ticket_description">Ticket Description</label>
        <textarea placeholder="Description" class="form-control" cols="10" rows="4" maxlength="200" name="ticket_description[]"></textarea>
    </div>
    <div class="form-group col-sm-6 col-lg-6">
        <div class="row">
            <div class="form-group col-sm-10 col-lg-10"> <small><i>Fees: (Our Fee: $0 and Buyer Pays: $0)</i></small>
                <select class="form-control" id="wizard-validation-skills" name="ticket_fees[]">
                    <option value="">Please select Fees Setting</option>
                    <option value="absorb fees">Absorb Fees</option>
                    <option value="pass on">Pass On</option>
                    
                </select>
                
            </div>
            <div class="form-group col-sm-10 col-lg-10">
                <label for="ticket_max">Maximum Ticket(s) per User:</label>
                <input placeholder="Maximum Ticket(s) per User" class="form-control" name="ticket_max[]" type="text">
            </div>
        </div>
    </div>
</div>
<!-- PAID TICKET on setting -->

</div>
<!-- clone paid dive end here -->
<!-- clone free dive start here -->
<div class="clone_free d-none"  style=" margin-top: 5px;   padding: 10px; ">
<div class="row ">
    <div class="form-group col-sm-4">
        
        <input placeholder="Name [ English ]" class="form-control" name="ticket_name[]" type="text">
    </div>
    <div class="form-group col-sm-3">
        
        <input placeholder="Total Quantity" class="form-control" name="ticket_quantity[]" type="text">
        
    </div>
    <div class="form-group col-sm-3">
        <label for="ticket_price">Free</label>
        
        <input type="hidden" name="ticket_price[]" value="0">
    </div>
    <div class=" form-group col-sm-1">
        
        <div class=" row ">
            <div class="col-md-4">
                <a href="javascript:void(0)"><i class="fa fa-cog fa-lg text-primary btnSetting"></i></a>
            </div> <div class="col-md-4">
            <a href="javascript:void(0)"><i class="fa fa-trash fa-lg text-danger btnRemove"></i></a>
        </div>
        
    </div>
    
</div>
</div>
<!-- free TICKET on setting -->
<div class="row d-none bg-light  other_div"  style="width:90%;">
<input type="hidden" value="free" name="">
<div class="form-group col-sm-6 col-lg-6">
    <label for="ticket_description">Ticket Description</label>
    <textarea placeholder="Description" class="form-control" cols="10" rows="4" maxlength="200" name="ticket_description[]"></textarea>
</div>
<div class="form-group col-sm-6 col-lg-6">
    <div class="row">
        <div class="form-group col-sm-10 col-lg-10"> <small><i>Fees: (Our Fee: $0 and Buyer Pays: $0)</i></small>
            <select class="form-control" id="wizard-validation-skills" name="ticket_fees[]">
                <option value="">Please select Fees Setting</option>
                <option value="1">Absorb Fees</option>
                <option value="2">Pass On</option>
                
            </select>
            
        </div>
        <div class="form-group col-sm-10 col-lg-10">
            <label for="ticket_max">Maximum Ticket(s) per User:</label>
            <input placeholder="Maximum Ticket(s) per User" class="form-control" name="ticket_max[]" type="text">
        </div>
    </div>
</div>
</div>
<!-- free TICKET on setting -->
</div>
<!-- clone  free dive end here -->
<!-- END Page Content -->
</main>
<!-- Footer -->
<footer id="page-footer" class="bg-body-light">
<div class="content py-3">
<div class="row font-size-sm">

<!-- <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
    
    Crafted with <i class="fa fa-heart text-danger"></i>
    
</div> -->

<div class="  col-sm-12 order-sm-1 py-1 text-center  ">
    
    &copy; <span data-toggle="year-copy"></span> <a class="font-w600" href="http://sports.voagstech.com/dashboard" target="_blank">Sports</a>
    
</div>

</div>
</div>
</footer>
<!-- END Footer -->
<!-- Apps Modal -->
<div class="modal fade" id="one-modal-apps" tabindex="-1" role="dialog" aria-labelledby="one-modal-apps" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">

<div class="block block-rounded block-themed block-transparent mb-0">
    
    <div class="block-header bg-primary-dark">
        
        <h3 class="block-title">Apps</h3>
        
        <div class="block-options">
            
            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
            
            <i class="si si-close"></i>
            
            </button>
            
        </div>
        
    </div>
    
    <div class="block-content block-content-full">
        
        <div class="row gutters-tiny">
            
            <div class="col-6">
                
                <!-- CRM -->
                
                <a class="block block-rounded block-link-shadow bg-body" href="javascript:void(0)">
                    
                    <div class="block-content text-center">
                        
                        <i class="si si-speedometer fa-2x text-primary"></i>
                        
                        <p class="font-w600 font-size-sm mt-2 mb-3">
                            
                            CRM
                            
                        </p>
                        
                    </div>
                    
                </a>
                
                <!-- END CRM -->
                
            </div>
            
            <div class="col-6">
                
                <!-- Items -->
                
                <a class="block block-rounded block-link-shadow bg-body" href="items">
                    
                    <div class="block-content text-center">
                        
                        <i class="si si-rocket fa-2x text-primary"></i>
                        
                        <p class="font-w600 font-size-sm mt-2 mb-3">
                            
                            Items
                            
                        </p>
                        
                    </div>
                    
                </a>
                
                <!-- END Items -->
                
            </div>
            
            <div class="col-6">
                
                <!-- Sales -->
                
                <a class="block block-rounded block-link-shadow bg-body mb-0" href="javascript:void(0)">
                    
                    <div class="block-content text-center">
                        
                        <i class="si si-plane fa-2x text-primary"></i>
                        
                        <p class="font-w600 font-size-sm mt-2 mb-3">
                            
                            Sales
                            
                        </p>
                        
                    </div>
                    
                </a>
                
                <!-- END Sales -->
                
            </div>
            
            <div class="col-6">
                
                <!-- Payments -->
                
                <a class="block block-rounded block-link-shadow bg-body mb-0" href="javascript:void(0)">
                    
                    <div class="block-content text-center">
                        
                        <i class="si si-wallet fa-2x text-primary"></i>
                        
                        <p class="font-w600 font-size-sm mt-2 mb-3">
                            
                            Payments
                            
                        </p>
                        
                    </div>
                    
                </a>
                
                <!-- END Payments -->
                
            </div>
            
        </div>
        
    </div>
    
</div>

</div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    $(document).ready(function(){

        $(".event_type").click(function(){ 
            var privacy_type=$(this).val();

            if(privacy_type=="public"){
                $('#category_type').removeClass('d-none');
                $('#link_type').addClass('d-none');
            }else if(privacy_type=='private'){
             $('#link_type').removeClass('d-none');
             $('#category_type').addClass('d-none');
         }

     });

        $(".ticket_add").click(function(){ 
            var type=$(this).data('ticket-type');
            if(type=='paid')
            {
               var html=$('.clone_paid').html();
               $("#variation_div").before(html);
           }
           else if(type=='free'){
               var html=$('.clone_free').html();
               $("#variation_div").before(html);
           }
       }); 

        $(document).on('click', '.btnRemove', function(){ 
            $(this).parent().parent().parent().parent().parent().remove();

        });

        $(document).on('click', '.btnSetting', function(){ 


if($(this).parent().parent().parent().parent().parent().next().hasClass('d-none')==false) //in actual it has this class
 {             $('.other_div').addClass('d-none')           
          //$(this).parent().parent().parent().parent().parent().next().toggleClass('d-none');
      }else{
         $('.other_div').not(this).addClass('d-none')  
         $(this).parent().parent().parent().parent().parent().next().removeClass('d-none');

     }
 });

    });
</script>

    
        
    
	 

	<script>

        function fetchdata(){
			
            $.ajax({
                type: 'GET', 
                url: 'https://voagstech.com/sports/orders/checkuserarrived',
                success: function (data) {
                      One.helpers('notify', {type: 'success',icon:'fa fa-coin',align: 'center', message: 'Record Deleted Successfully'});
                    if(!(data))
                    {
                        $('#notification').css("display","none");
                        $('#dothdn').css("display","none");
                        $('#notificationnone').css("display","inline");
                    }
                    else
                    {
                        $('#notificationnone').css("display","none");
                        $('#notification').html(data).css("display","inline");
                        $('#dothdn').css("display","inline");
                    }
                }
            });
        }

        $(document).ready(function(){
           /* fetchdata();
            setInterval(fetchdata,5000);*/
           jQuery(function(){One.helpers(['table-tools-checkable', 'table-tools-sections']);}); 
        });        

    </script>
@endsection