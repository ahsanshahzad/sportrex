@extends('layout.dashboard')
@section('content')
<!-- Sidebar -->
@include('includes.dashboard.sidebar')
<!-- navbar -->
@include('includes.dashboard.navbar')
<main id="main-container">
    <div class="bg-body-light">
        <div class="content">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                Manage Events
                </h1>
            </div>
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center pt-5">
                <nav class="flex-sm-00-auto" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a class="link-fx" href="https://voagstech.com/sports/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            Event Listing
                        </li>
                    </ol>
                </nav>
                
                <!--  <a href="#" data-toggle="modal" data-target="#createModal" class="btn btn-primary btn-add-new pull-right">
                    <i class="fa fa-plus-square fa-lg"></i> Add New
                </a> -->
            </div>
        </div>
    </div>
    <!-- END Hero -->
    <!-- Page Content -->
    <div class="content">
        <!-- Active event tab -->
        <div class="block block-mode-hidden block-rounded block-themed">
            <div class="block-header">
                <h3 class="block-title">Active Events <small></small></h3>
                <div class="block-options">
                    <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                </div>
            </div>
            <div class="block-content">
                <form method="post" role="form" id="data-search-form">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover"  id="upcomingDataTable">
                            <thead>
                                <tr role="row" class="heading" colspan="4">
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="text" class="form-control" id="su_event" autocomplete="off" placeholder="Search Event">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="su_venue" autocomplete="off" placeholder="Search Venue">
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <!--  <td>
                                        <input type="date" class="form-control" id="s_date" autocomplete="off" >
                                    </td> -->
                                    <tr>
                                        <th>#</th>
                                        <th>Event Name</th>
                                        <th>Venue</th>
                                        <th>Start Date</th>
                                        <th>Start Time</th>
                                        <th>End Date</th>
                                        <th>End Time</th>
                                        <!--   <th>Status</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($activeEvents)
                                    @foreach($activeEvents as $ae)
                                        <tr>
                                            <td>{{$ae->id}}</td>
                                            <td>{{$ae->title}}</td>
                                            <td>{{$ae->venue}}</td>
                                            <td>{{$ae->start_date}}</td>
                                            <td>{{$ae->start_time}}</td>
                                            <td>{{$ae->end_date}}</td>
                                            <td>{{$ae->end_time}}</td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Horizontal Info">
                                                    <!-- <a class="btn btn-outline-primary" href="#" title="View Details">
                                            			<i class="fa fa-edit"></i>
                                            		</a>  -->
                                                    <a class="btn btn-outline-primary" href="javascript:;" onclick="confirmDelete('{{$ae->id}}')">
                                            			<i class="fa fa-trash fa-lg text-danger btnRemove"></i>
                                            		</a>
                                                    
                                                            <form id="delete-blog-{{$ae->id}}"
                                                                  action="{{ route('events.destroy', $ae->id) }}"
                                                                  method="POST" style="display: none;">
                                                                @method('DELETE')
                                                                @csrf
                                                            </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
        </div>
        <!-- active event tab -->
        <!-- 2. Upcoming event tab -->
        <div class="block block-mode-hidden block-rounded block-themed">
            <div class="block-header">
                <h3 class="block-title">Upcoming Events <small></small></h3>
                <div class="block-options">
                    <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                </div>
            </div>
            <div class="block-content">
                <form method="post" role="form" id="data-search-form">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover"  id="upcomingDataTable">
                            <thead>
                                <tr role="row" class="heading" colspan="4">
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="text" class="form-control" id="su_event" autocomplete="off" placeholder="Search Event">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="su_venue" autocomplete="off" placeholder="Search Venue">
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <!--  <td>
                                        <input type="date" class="form-control" id="s_date" autocomplete="off" >
                                    </td> -->
                                    <tr>
                                        <th>#</th>
                                        <th>Event Name</th>
                                        <th>Venue</th>
                                        <th>Start Date</th>
                                        <th>Start Time</th>
                                        <th>End Date</th>
                                        <th>End Time</th>
                                        <!--   <th>Status</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($upcomingEvents)
                                    @foreach($upcomingEvents as $ue)
                                    <tr>
                                        <td></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
            <!-- 2. Upcoming event tab -->
            <!-- 3. Past Events -->
            <div class="block block-mode-hidden block-rounded block-themed">
                <div class="block-header">
                    <h3 class="block-title">Past Events <small></small></h3>
                    <div class="block-options">
                        <!-- To toggle block's content, just add the following properties to your button: data-toggle="block-option" data-action="content_toggle" -->
                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                    </div>
                </div>
                <div class="block-content">
                    <form method="post" role="form" id="data-search-form">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover"  id="pastDataTable">
                                <thead>
                                    <tr role="row" class="heading" colspan="4">
                                        <td>&nbsp;</td>
                                        <td>
                                            <input type="text" class="form-control" id="sp_event" autocomplete="off" placeholder="Search Event">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="sp_venue" autocomplete="off" placeholder="Search Venue">
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <!--  <td>
                                            <input type="date" class="form-control" id="s_date" autocomplete="off" >
                                        </td> -->
                                        <tr>
                                            <th>#</th>
                                            <th>Event Name</th>
                                            <th>Venue</th>
                                            <th>Start Date</th>
                                            <th>Start Time</th>
                                            <th>End Date</th>
                                            <th>End Time</th>
                                            <!--   <th>Status</th>-->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if($pastEvents)
                                    @foreach($pastEvents as $pe)
                                        <tr>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
            <div class="modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="block block-rounded block-themed mb-0">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Add New</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option btn_close_modal" data-bs-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-fw fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <form method="POST" action="https://voagstech.com/sports/events" accept-charset="UTF-8" enctype="multipart/form-data"><input name="_token" type="hidden" value="BpC83y02vjnyalroVew5fG2pVAvpVfdsbmzVrdYs">
                            <div class="block-content fs-sm">
                                <!-- include('events.fields') -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Footer -->

        <footer id="page-footer" class="bg-body-light">

            <div class="content py-3">

                <div class="row font-size-sm">

                    <!-- <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">

                        Crafted with <i class="fa fa-heart text-danger"></i>

                    </div> -->

                    <div class="  col-sm-12 order-sm-1 py-1 text-center  ">

                        &copy; <span data-toggle="year-copy"></span> <a class="font-w600" href="https://voagstech.com/sports/dashboard" target="_blank">Sports</a>

                    </div>

                </div>

            </div>

        </footer>

        <!-- END Footer -->
    @endsection
    @section('scripts')
    <script>
        function confirmDelete(id) {
            let choice = confirm('Are you sure, You want to delete ?');
            if (choice) {
                document.getElementById('delete-blog-' + id).submit();
            }
        }
    </script>
    @endsection