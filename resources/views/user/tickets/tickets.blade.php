@extends('layout.dashboard')
@section('content')
<!-- Sidebar -->
@include('includes.dashboard.sidebar')
<!-- navbar -->
@include('includes.dashboard.navbar')
<main id="main-container">
    
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                Tickets Dashboard
                </h1>
            </div>
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <nav class="flex-sm-00-auto" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a class="link-fx" href="https://voagstech.com/sports/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            Tickets Listing
                        </li>
                    </ol>
                </nav>
                
            </div>
        </div>
    </div>
    <!-- END Hero -->
    
    <!-- Page Content -->
    <div class="content">
        
        <div class="row justify-content-center">
            <div class="col-md-12 col-xl-12">
                <div class="block">
                    <div class="block-content">
                        <h3 class="block-title mb-3">Tickets Summary Listing</h3>
                        
                        <div class="table-responsive">
                            <table class="table table-striped table-hover"  id="myDataTable">
                            <thead>
                                <tr role="row" class="heading" colspan="4">
                                    <td>&nbsp;</td>
                                    <td>
                                        <input type="text" class="form-control" id="su_event" autocomplete="off" placeholder="Search Event">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="su_venue" autocomplete="off" placeholder="Search Venue">
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <!--  <td>
                                        <input type="date" class="form-control" id="s_date" autocomplete="off" >
                                    </td> -->
                                    <tr>
                                        <th>#</th>
                                        <th>Event Name</th>
                                        <th>Venue</th>
                                        <th>Start Date</th>
                                        <th>Start Time</th>
                                        <th>End Date</th>
                                        <th>End Time</th>
                                        <!--   <th>Status</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
        </div>
        <!-- active event tab -->
            
        </div>
    </main>
    <!-- Footer -->

        <footer id="page-footer" class="bg-body-light">

            <div class="content py-3">

                <div class="row font-size-sm">

                    <!-- <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">

                        Crafted with <i class="fa fa-heart text-danger"></i>

                    </div> -->

                    <div class="  col-sm-12 order-sm-1 py-1 text-center  ">

                        &copy; <span data-toggle="year-copy"></span> <a class="font-w600" href="https://voagstech.com/sports/dashboard" target="_blank">Sports</a>

                    </div>

                </div>

            </div>

        </footer>

        <!-- END Footer -->
    @endsection
    @section('scripts')
    
    @endsection