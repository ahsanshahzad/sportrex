            <div class="sec_1 page4 ff-sec"  style="   background: url('assets/images/sp1.png');background-size: cover!important;background: linear-gradient(30deg,rgba(0, 0,0, 0.8), rgba(0, 0, 0, 0.2)),url({{asset('public/assets/images/sp1.png')}});">
                <nav class="navbar navbar-expand-lg navbar-light " style="max-width: 1200px;
    margin: auto;
    width: 100%;">
                    <a class="navbar-brand" href="{{url('/')}}"><img class = "logo_img" width="140" src="{{asset('public/assets/images/logo/logo.png')}}" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                      <!-- <span class="navbar-toggler-icon"></span> -->
                      <i style="color: #fff;" class="fas fa-bars menu-icon"></i>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                      <ul class="navbar-nav">
                        <li class="it1 nav-item active">
                            <a class="nav-link" href="#" style="color: #fff !important;">Find Event <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="it2 nav-item">
                          <a class="nav-link" href="#" style="color: #fff!important;">Retrive Ticket</a>
                        </li>
                        <li class="it3 nav-item">
                          <a class="nav-link" href="#" style="color: #fff!important;">About</a>
                        </li>
                        <li class="it4 nav-item">
                            <a class="nav-link " href="{{route('contact')}}" style="color: #fff!important;">Contact</a>
                        </li>
        
                        <li class="it5">
                            <a class="nav-link nav_padding-left" href="{{ route('register') }}" style="color: #fff!important;">Become an organizer</a>
                          </li>
        
                          <li class="it6 nav-item">
                            <a class="nav-link sign_in_button" href="{{route('login')}}" style="color: #fff!important;     border-color: white;">Sign in</a>
                          </li>
                      </ul>
                    </div>
                  </nav>
                  
             <!--owl Slider 3-->
             <div class="sec4 page4 f-sec" >
                <div class="row">
                    <div class="col-md-12">
                            <div id="home-title" class="titleBox">
                @if($category)
                <h6 style="color: #fff; font-size: 15px;">Home / {{$category->title}}</h6>
                <h3 class = "text-white">{{$category->title}}</h3>

                
                <h4 class = "text-white">Join Your favourite Football events today.</h4>
                <hr style="color: #fff;">
                <div class="d-flex align-items-center">
                                        <select class="text-black col-xxl-6 col-md-2 no-border myDropdown" id="myDropdown" style="    font-size: 14px !important;
background: transparent;color: white;"  name="address" placeholder="Location">
                        <option selected>Location</option>
                        <option value="George Town">George Town</option>
                        <option value="Kuala Lumpur">Kuala Lumpur</option>
                        <option value="Seberang Perai">Seberang Perai</option>
                        <option value="Johor Bahru">Johor Bahru</option>
                        <option value="Ipoh">Ipoh</option>
                        <option value="Seremban">Seremban</option>
                        <option value="Malacca">Malacca</option>
                        <option value="Kota Kinabalu">Kota Kinabalu</option>
                        <option value="Kuantan">Kuantan</option>
                        <option value="Alor Setar">Alor Setar</option>
                        <option value="Kuala Terengganu">Kuala Terengganu</option>
                        <option value="Kuching">Kuching</option>
                        <option value="Miri">Miri</option>
                        <option value="Subang Jaya">Subang Jaya</option>
                        <option value="Petaling Jaya">Petaling Jaya</option>
                        <option value="Shah Alam">Shah Alam</option>
                        <option value="Pasir Gudang">Pasir Gudang</option>
                        <option value="Iskandar Puteri">Iskandar Puteri</option>
                  </select>             
                  <select class="text-black col-xxl-6 col-md-2 no-border myDropdown" style="    font-size: 14px !important;
background: transparent;color: white;"  name="time">
                      <option>All time</option>
                      <option value="00:00">00:00</option>
                      <option value="01:00">01:00</option>
                      <option value="02:00">02:00</option>
                      <option value="03:00">03:00</option>
                      <option value="04:00">04:00</option>
                      <option value="05:00">05:00</option>
                      <option value="06:00">06:00</option>
                      <option value="07:00">07:00</option>
                      <option value="08:00">08:00</option>
                      <option value="09:00">09:00</option>
                      <option value="10:00">10:00</option>
                      <option value="11:00">11:00</option>
                      <option value="12:00">12:00</option>
                      <option value="13:00">13:00</option>
                      <option value="14:00">14:00</option>
                      <option value="15:00">15:00</option>
                      <option value="16:00">16:00</option>
                      <option value="17:00">17:00</option>
                      <option value="18:00">18:00</option>
                      <option value="19:00">19:00</option>
                      <option value="20:00">20:00</option>
                      <option value="21:00">21:00</option>
                      <option value="22:00">22:00</option>
                      <option value="23:00">23:00</option>
                  </select>
                </div>
                @endif                                
                              
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>