<header id="page-header">
	<div class="content-header">
		
		<div class="d-flex align-items-center">
			
			<button type="button" class="btn btn-sm btn-dual mr-2 d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
			
			<i class="fa fa-fw fa-bars"></i>
			
			</button>
			
			<button type="button" class="btn btn-sm btn-dual mr-2 d-none d-lg-inline-block" data-toggle="layout" data-action="sidebar_mini_toggle">
			
			<i class="fa fa-fw fa-ellipsis-v"></i>
			
			</button>
			
		</div>
		<div class="d-flex align-items-center">
			
			
			<div class="dropdown d-inline-block ms-2">
				<button type="button" class="btn btn-sm btn-alt-secondary" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
				<i class="fas fa-bell"></i>
				<span class="text-primary"  id="dothdn" style="display: none">•</span>
				</button>
				
				<div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0 border-0 fs-sm" aria-labelledby="page-header-notifications-dropdown" style="">
					<div class="p-2 bg-body-light border-bottom text-center rounded-top">
						<h5 class="dropdown-header text-uppercase">Notifications</h5>
					</div>
					<ul class="nav-items mb-0" id="notification">
						
					</ul>
					
					<ul class="nav-items mb-0" id="notificationnone" style="display:none;">
						<li >
							<a class="text-dark d-flex py-2 ml-4 " href="javascript:void(0)">
								<div class="flex-shrink-0 me-2 ms-3 ">
									<span class="">
										<i class="fa fa-fw fa-times-circle text-danger"></i>
										<span class="fw-semibold">Notification is not available</span>
									</span>
								</div>
							</a>
						</li>
					</ul>
					
					
					<div class="p-2 border-top text-center" style="display: none;" id="loadmore">
						<a class="d-inline-block fw-medium" href="https://sports.voagstech.com/orders/checkuserarrived">
							<i class="fa fa-fw fa-arrow-down me-1 opacity-50"></i> Load More..
						</a>
					</div>
				</div>
			</div>
			
			<div class="dropdown d-inline-block ml-2">
				
				<button type="button" class="btn btn-sm btn-dual d-flex align-items-center" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				
				<img class="rounded-circle" src="{{asset('assets//media/avatars/avatar10.jpg')}}" alt="Header Avatar" style="width: 21px;">
				
				
				<i class="fa fa-fw fa-angle-down d-none d-sm-inline-block ml-1 mt-1"></i>
				
				</button>
				
				<div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 border-0" aria-labelledby="page-header-user-dropdown">
					
					<div class="p-3 text-center bg-primary-dark rounded-top">
						
						<img class="img-avatar img-avatar48 img-avatar-thumb" src="{{asset('assets//media/avatars/avatar10.jpg')}}" alt="">
						
						<p class="mt-2 mb-0 text-white font-w500">Manager Sport Complex</p>
						
						<!--  <p class="mb-0 text-white-50 font-size-sm"></p> -->
						
					</div>
					
					<div class="p-2">
						
						<a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
							
							<span class="font-size-sm font-w500">Profile</span>
							
							
							
						</a>
						
						<div role="separator" class="dropdown-divider"></div>
						
						<a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							
							<span class="font-size-sm font-w500">Log Out</span>
							
						</a>
						
						<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
							@csrf
						</form>
						
					</div>
					
				</div>
				
			</div>
		</div>
		
	</div>
	<div id="page-header-loader" class="overlay-header bg-white">
		
		<div class="content-header">
			
			<div class="w-100 text-center">
				
				<i class="fa fa-fw fa-circle-notch fa-spin"></i>
				
			</div>
			
		</div>
		
	</div>
	
</header>
<aside id="side-overlay" class="font-size-sm">
    
    <div class="content-header border-bottom">
        
        <a class="img-link mr-1" href="javascript:void(0)">
            
            <img class="img-avatar img-avatar32" src="http://sports.voagstech.com/assets//media/avatars/avatar10.jpg" alt="">
            
        </a>
        
        <div class="ml-2">
            
            <a class="text-dark font-w600 font-size-sm" href="javascript:void(0)">Manager Sport Complex</a>
            
        </div>
        
        <a class="ml-auto btn btn-sm btn-alt-danger" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_close">
            
            <i class="fa fa-times"></i>
            
        </a>
        
    </div>
    
    <div class="content-side">
        
        <h6>Profile Details</h6>
        
        <ul class="list-group mb-5">
            
            <li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-user-tie"></i></span><span class="bg-light rounded px-2">Manager Sport Complex</span></li>
            
            <li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-briefcase"></i></span><span class="bg-light rounded px-2"></span></li>
            
            <li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-phone"></i></span><span class="bg-light rounded px-2">+923068672409</span></li>
            
            <li class="list-group-item d-flex justify-content-between align-items-center"><span class="text-muted"><i class="mr-2 fa fa-envelope"></i></span><span class="bg-light rounded px-2">admin@gmail.com</span></li>
            
        </ul>
        
        
        
        <h6>Change Password</h6>
        
        <form class="bg-light rounded px-4 py-2" action="http://sports.voagstech.com/users/updatePassword" method="POST">
            @csrf
            <input type="hidden" name="_token" value="uFoSgPXves1fDbJnA66W6YFiFa7hjsGUOGFRAOHz">
            <div class="my-2">
                
                <label for="currentPass">Current Password</label>
                
                <input type="text" class="form-control" name="current_password" id="currentPass">
                
            </div>
            
            
            
            <div class="my-2">
                
                <label for="newPass">New Password</label>
                
                <input type="text" class="form-control" name="new_password" id="newPass">
                
            </div>
            
            
            
            <div class="mt-2 mb-3">
                
                <input type="submit" value="Change Password" class="btn btn-dark btn-block">
                
            </div>
            
        </form>
        
    </div>
    
</aside>