<nav id="sidebar" aria-label="Main Navigation">
                <div class="content-header bg-white-5">
                    <a class="font-w600 text-dual" href="{{url('/')}}">
                        <span class="smini-visible">
                            <i class="fa fa-circle-notch text-primary"></i>
                        </span>
                        <span class="smini-hide font-size-h5 tracking-wider">
                            <!--  Sports -->
                        <img class = "logo_img" style="width:150px" src="{{asset('public/assets/images/logo/logo.png')}}" alt=""></a>
                        <!--   ka -->
                        <span class="font-w400"></span>
                    </span>
                </a>
                <div>
                    <div class="dropdown d-inline-block ml-2">
                        <a class="btn btn-sm btn-dual" id="sidebar-themes-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                            <i class="si si-drop"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right font-size-sm smini-hide border-0" aria-labelledby="sidebar-themes-dropdown">
                            <a class="dropdown-item d-flex align-items-center justify-content-between font-w500" href="https://sports.voagstech.com/users/default/set-theme">
                                <span>Default</span>
                                <i class="fa fa-circle text-default"></i>
                            </a>
                            <a class="dropdown-item d-flex align-items-center justify-content-between font-w500" href="https://sports.voagstech.com/users/amethyst/set-theme">
                                <span>Amethyst</span>
                                <i class="fa fa-circle text-amethyst"></i>
                            </a>
                            <a class="dropdown-item d-flex align-items-center justify-content-between font-w500" href="https://sports.voagstech.com/users/city/set-theme">
                                <span>City</span>
                                <i class="fa fa-circle text-city"></i>
                            </a>
                            <a class="dropdown-item d-flex align-items-center justify-content-between font-w500" href="https://sports.voagstech.com/users/flat/set-theme">
                                <span>Flat</span>
                                <i class="fa fa-circle text-flat"></i>
                            </a>
                            <a class="dropdown-item d-flex align-items-center justify-content-between font-w500" href="https://sports.voagstech.com/users/modern/set-theme">
                                <span>Modern</span>
                                <i class="fa fa-circle text-modern"></i>
                            </a>
                            <a class="dropdown-item d-flex align-items-center justify-content-between font-w500" href="https://sports.voagstech.com/users/smooth/set-theme">
                                <span>Smooth</span>
                                <i class="fa fa-circle text-smooth"></i>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item font-w500" href="https://sports.voagstech.com/users/light/set-sidebar">
                                <span>Sidebar Light</span>
                            </a>
                            <a class="dropdown-item font-w500" href="https://sports.voagstech.com/users/dark/set-sidebar">
                                <span>Sidebar Dark</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item font-w500" href="https://sports.voagstech.com/users/light/set-header">
                                <span>Header Light</span>
                            </a>
                            <a class="dropdown-item font-w500" href="https://sports.voagstech.com/users/dark/set-header">
                                <span>Header Dark</span>
                            </a>
                        </div>
                    </div>
                    <a class="d-lg-none btn btn-sm btn-dual ml-1" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                        <i class="fa fa-fw fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="js-sidebar-scroll">
                <div class="content-side">
                    <ul class="nav-main">
                        <li class="nav-main-item">
                            <a class="nav-main-link @if(Request::url() === url('/dashboard')) ? active :  @endif" href="{{url('/dashboard')}}">
                                <i class="nav-main-link-icon si si-home"></i>
                                <span class="nav-main-link-name">Dashboard</span>
                            </a>
                        </li>
                        
                        
                        <li class="nav-main-item">
                            
                            <a class="nav-main-link @if(Request::url() === url('event/create')) ? active :  @endif" href="{{url('event/create')}}">
                                <i class="nav-main-link-icon fa fa-calendar"></i>
                                <span class="nav-main-link-name">Create New Event</span>
                                
                            </a>
                            
                        </li>
                        
                        <!--    -->
                        <!-- my events -->
                        <li class="nav-main-item @if(Request::url() === route('events.index') || Request::url() === route('sales.report') || 
                            Request::url() === route('ticket.report')) ? open :  @endif">
                            
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                
                                <i class="nav-main-link-icon si si-social-dropbox"></i>
                                
                                <span class="nav-main-link-name">My Events</span>
                                
                            </a>
                            
                            <ul class="nav-main-submenu">
                                
                                <!--   -->
                                
                                <li class="nav-main-item">
                                    
                                    <a class="nav-main-link @if(Request::url() === route('events.index')) ? active :  @endif" href="{{route('events.index')}}">
                                        
                                        <span class="nav-main-link-name">Manage Events </span>
                                        
                                    </a>
                                    
                                </li>
                                
                                <!--  -->
                                
                                <!--  -->
                                
                                <li class="nav-main-item">
                                    
                                    <a class="nav-main-link @if(Request::url() === route('sales.report')) ? active :  @endif" href="{{route('sales.report')}}">
                                        
                                        <span class="nav-main-link-name">Sales Report</span>
                                        
                                    </a>
                                    
                                </li>
                                
                                <!--   -->
                                
                                
                                <!--  -->
                                
                                <li class="nav-main-item">
                                    
                                    <a class="nav-main-link @if(Request::url() === route('ticket.report')) ? active :  @endif" href="{{route('ticket.report')}}">
                                        
                                        <span class="nav-main-link-name">Ticket Report</span>
                                        
                                    </a>
                                    
                                </li>
                                
                                <!--   -->
                                
                            </ul>
                            
                        </li>
                        <!-- my events -->
                        <!--      -->
                        
                        
                        <li class="nav-main-item">
                            <a class="nav-main-link @if(Request::url() === route('coupons.index')) ? active :  @endif" href="{{route('coupons.index')}}">
                                <i class="nav-main-link-icon fa fa-gift"></i>
                                <span class="nav-main-link-name">Coupons</span>
                            </a>
                        </li>
                        
                        
                        <!--    -->
                        <!-- invitations -->
                        <!-- <li class="nav-main-item ">
                            
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                
                                <i class="nav-main-link-icon fa fa-envelope"></i>
                                
                                <span class="nav-main-link-name">Inviation & Promote</span>
                                
                            </a>
                            
                            <ul class="nav-main-submenu">
                                
                                <!--   -->
                                
                                <li class="nav-main-item">
                                    
                                    <a class="nav-main-link " href="http://sports.voagstech.com/dashboard">
                                        
                                        <span class="nav-main-link-name">Private Invitation </span>
                                        
                                    </a>
                                    
                                </li>
                                
                                <!--  -->
                                
                                
                                
                            </ul>
                            
                        </li> -->
                        <!-- inviations -->
                        <!--      -->
                        
                        
                        <!-- <li class="nav-main-item">
                            <a class="nav-main-link " href="http://sports.voagstech.com/dashboard">
                                <i class="nav-main-link-icon fa fa-network-wired"></i>
                                <span class="nav-main-link-name">Affiliate</span>
                            </a>
                        </li> -->
                        
                        
                        <li class="nav-main-item">
                            <a class="nav-main-link " href="http://sports.voagstech.com/dashboard">
                                <i class="nav-main-link-icon fa fa-money-check-alt"></i>
                                <span class="nav-main-link-name">Payout</span>
                            </a>
                        </li>
                        
                        
                        
                        <!-- <li class="nav-main-item">
                            <a class="nav-main-link " href="http://sports.voagstech.com/dashboard">
                                <i class="nav-main-link-icon fa fa-user"></i>
                                <span class="nav-main-link-name">Access Manager</span>
                            </a>
                        </li> -->
                        
                        
                        
                        <!--  <li class="nav-main-item">
                            
                            <a class="nav-main-link " href="http://sports.voagstech.com/main-categories">
                                <i class="nav-main-link-icon si si-layers"></i>
                                <span class="nav-main-link-name">Categories</span>
                            </a>
                            
                        </li>  -->
                        
                        
                        
                        <!--                           <li class="nav-main-item">
                            
                            <a class="nav-main-link " href="http://sports.voagstech.com/products">
                                <i class="nav-main-link-icon si si-layers"></i>
                                <span class="nav-main-link-name">Products</span>
                                
                            </a>
                            
                        </li>
                        -->
                        
                        
                        
                        
                        
                        
                        <!--   -->
                        
                        <li class="nav-main-item ">
                            
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                
                                <i class="nav-main-link-icon fa fa-lock"></i>
                                
                                <span class="nav-main-link-name">My Account</span>
                            </a>
                            
                            <ul class="nav-main-submenu">
                                
                                <!--   -->
                                
                                <li class="nav-main-item">
                                    
                                    <a class="nav-main-link "  href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                                        
                                        <span class="nav-main-link-name">Profile </span>
                                    </a>
                                </li>
                                
                                <!--    -->
                                
                                <!--                         -->
                                <li class="nav-main-item">
                                    
                                    <a class="nav-main-link "  href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle" >
                                        
                                        <span class="nav-main-link-name">Change Password</span>
                                        
                                    </a>
                                    
                                </li>
                                
                                <!--    -->
                                
                            </ul>
                            
                        </li>
                        <!--
                        -->
                        
                    </ul>
                    
                </div>
                
            </div>
            
        </nav>