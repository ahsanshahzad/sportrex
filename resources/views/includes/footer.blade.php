
<div class="sec6_bg page6footer">

            <div class="sec6">
                <div class="row footer_margin_top">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <img class="footer_logo" src="{{asset('public/assets/images/logo/logo.png')}}" alt="">
                        <p class="footer_paragraph">Sportrex is a global self-service ticketing platform
                            for live experiences that enables organisers to
                            create, share, find, and attend sports events that
                            fuel their passions and enrich their lives. From music
                            festivals, marathons, sports tournament, classes,
                            gaming competitions and many more. Our mission is
                            to bring the sports community together through live
                            experiences.</p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 footer_grid">
                                            <!-- Links -->
                        <h5 class="links_heading_1">Helpful Links</h5>

                        <ul class="list-unstyled">
                        <li>
                            <a class="link_a" href="{{route('register')}}">Sign Up</a>
                        </li>
                        <li>
                            <a class="link_a" href="{{route('login')}}">Login</a>
                        </li>
                        <li>
                            <a class="link_a" href="#findeventsection">Find Events</a>
                        </li>
                        <li>
                            <a class="link_a" href="#!">Retrieve Ticket</a>
                        </li>
                       
                        <li>
                            <a class="link_a" href="{{route('register')}}">Become an Organizer</a>
                        </li>

                        <li>
                            <a class="link_a" href="#!">FAQ
                            </a>
                        </li>
                        </ul>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 footer_grid">

                             <!-- Links 2 -->
                        <h5 class="links_heading_1">Company</h5>

                        <ul class="list-unstyled">
                        <li>
                            <a class="link_a" href="#!">About</a>
                        </li>
                        <li>
                            <a class="link_a" href="{{route('contact')}}">Contact us</a>
                        </li>
                        <li>
                            <a class="link_a" href="#!">Terms</a>
                        </li>
                        <li>
                            <a class="link_a" href="#!">Privacy Policy</a>
                        </li>
                       
                       

                        </ul>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 footer_grid">
                            
                             <!-- Links 3 -->
                        <h5 class="links_heading_1">Connect with us</h5>

                        <ul class="list-unstyled">
                        <li>
                            <a class="link_a" href="#!">Whatsapp</a>
                        </li>
                        <li>
                            <a class="link_a" href="#!">E-Mail</a>
                        </li>
                        <li>
                            <a class="link_a" href="#!"><img class = "in_icon" src="{{asset('public/assets/images/in.png')}}" alt=""></a>
                            <a class="link_a" href="#!"><img class = "fb_icon" src="{{asset('public/assets/images/fb.png')}}" alt=""></a>
                        </li>
                       
                       

                        </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <hr class = "line"></hr>
                            </div></div>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p class="rights">@ 2021 Sportrex.com.my. All Rights Reserved</p>
                                    </div></div>



            </div>

            </div>