
<div class="sec_1 page2">
  <div class="whitenav">
        <nav class="navbar navbar-expand-lg navbar-light  container ">
            <a class="navbar-brand" href="{{url('/')}}"><img class = "logo_img" width="140px" src="{{asset('public/assets/images/logo/logo1.PNG')}}" alt="Sportrex"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <!-- <span class="navbar-toggler-icon"></span> -->
              <i style="color: #000;" class="fas fa-bars menu-icon"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="it1 nav-item active">
                  <a class="nav-link" href="#">Find Event <span class="sr-only">(current)</span></a>
                </li>
                <li class="it2 nav-item">
                  <a class="nav-link" href="#">Retrive Ticket</a>
                </li>
                <li class="it3 nav-item">
                  <a class="nav-link" href="#">About</a>
                </li>
                <li class="it4 nav-item">
                    <a class="nav-link " href="{{route('contact')}}">Contact</a>
                </li>

                <li class="it5">
                    <a class="nav-link nav_padding-left" href="{{ route('register') }}">Become an organizer</a>
                  </li>

                  <li class="it6 nav-item">
                    <a class="nav-link sign_in_button" href="{{route('login')}}">Sign in</a>
                  </li>
              </ul>
            </div>
          </nav>
              
              
          </div>

</div>