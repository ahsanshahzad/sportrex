<style>
    .buttonddd{
        width: 122px !important; border: 0 !important;    background: transparent !important;
    }
@media only screen and (max-width:500px)
{
    .buttonddd{
        width: 60px !important; border: 0 !important;    background: transparent !important;
    } 
}
    .dfd{
        margin:8px !important;padding: 0 !important;height: 23px !important;font-size: 14px !important; border:0 !important;background: white !important;color: black !important;;
    }
@media only screen and (max-width:500px)
{
    .dfd{
        margin:8px !important;padding: 0 !important;height: 23px !important;font-size: 6px !important;border:0 !important;background: white !important;color: black !important;;
    } 
}
</style>
<div class="sec_1 page1">
            <nav class="navbar navbar-expand-lg navbar-light container">
                <a class="navbar-brand" href="{{url('/')}}"><img class = "logo_img" width="140" src="{{asset('public/assets/images/logo/logo.png')}}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <!-- <span class="navbar-toggler-icon"></span> -->
                  <i style="color: #fff;" class="fas fa-bars menu-icon"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="it1 nav-item active">
                      <a class="nav-link" id="findevent"  href="#findeventsection">Find Event <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="it2 nav-item it2">
                      <a class="nav-link" href="#">Retrive Ticket</a>
                    </li>
                    <li class="it3 nav-item">
                      <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="it4 nav-item">
                      <a class="nav-link " href="{{route('contact')}}">Contact</a>
                    </li>

                    <li class="it5">
                        <a class="nav-link nav_padding-left" href="{{ route('register') }}">Become an organizer</a>
                      </li>

                      <li class="nav-item it6">
                        <a class="nav-link sign_in_button" href="{{ route('login') }}">Sign in</a>
                      </li>
                  </ul>
                </div>
              </nav>
              <div class="center">
              <img class  = "white_img" src="{{asset('public/assets/images/white_s.png')}}" alt="">
              <h3 class = "nav_title">Find Nearby Sport Events</h3>
              <h4 class = "nav_title_1">Explore what's happening, where and when</h4>
              
              <!--Search Bar-->
              <form method="get" action="{{route('index.search')}}">
                <div class="d-flex justify-content-center">
                <div class="searchbar d-flex justify-content-between align-items-center">
                  
                  <input class="search_input dfd" type="text" name="search"  placeholder="Search for an event">
                  <select class="text-black col-3 no-border dfd" id="myDropdown"  name="address" placeholder="Location">
                        <option selected>Location</option>
                        <option value="George Town">George Town</option>
                        <option value="Kuala Lumpur">Kuala Lumpur</option>
                        <option value="Seberang Perai">Seberang Perai</option>
                        <option value="Johor Bahru">Johor Bahru</option>
                        <option value="Ipoh">Ipoh</option>
                        <option value="Seremban">Seremban</option>
                        <option value="Malacca">Malacca</option>
                        <option value="Kota Kinabalu">Kota Kinabalu</option>
                        <option value="Kuantan">Kuantan</option>
                        <option value="Alor Setar">Alor Setar</option>
                        <option value="Kuala Terengganu">Kuala Terengganu</option>
                        <option value="Kuching">Kuching</option>
                        <option value="Miri">Miri</option>
                        <option value="Subang Jaya">Subang Jaya</option>
                        <option value="Petaling Jaya">Petaling Jaya</option>
                        <option value="Shah Alam">Shah Alam</option>
                        <option value="Pasir Gudang">Pasir Gudang</option>
                        <option value="Iskandar Puteri">Iskandar Puteri</option>
                  </select>
                  <select class="text-black col-3 no-border dfd"  name="time">
                      <option>All time</option>
                      <option value="00:00">00:00</option>
                      <option value="01:00">01:00</option>
                      <option value="02:00">02:00</option>
                      <option value="03:00">03:00</option>
                      <option value="04:00">04:00</option>
                      <option value="05:00">05:00</option>
                      <option value="06:00">06:00</option>
                      <option value="07:00">07:00</option>
                      <option value="08:00">08:00</option>
                      <option value="09:00">09:00</option>
                      <option value="10:00">10:00</option>
                      <option value="11:00">11:00</option>
                      <option value="12:00">12:00</option>
                      <option value="13:00">13:00</option>
                      <option value="14:00">14:00</option>
                      <option value="15:00">15:00</option>
                      <option value="16:00">16:00</option>
                      <option value="17:00">17:00</option>
                      <option value="18:00">18:00</option>
                      <option value="19:00">19:00</option>
                      <option value="20:00">20:00</option>
                      <option value="21:00">21:00</option>
                      <option value="22:00">22:00</option>
                      <option value="23:00">23:00</option>
                  </select>
                  <button type="submit" class="search_icon buttonddd col-1"><img class="search_icon" src="{{asset('public/assets/images/search.png')}}"  alt=""></button>
                </div>
              </div>              
              </form>
              
              
              
              
            </div>

        </div>