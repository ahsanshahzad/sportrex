<html>
    <head>
        <title>Sportrex</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
        <link rel="stylesheet" href="{{asset('public/assets/css/carousel.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
        <link rel="stylesheet" href="./lib/font-awesome/css/all.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}">
        <style type="text/css">
            .register_button {
                
                padding-left: 7vw;
            }
        </style>
    </head>
    <body>
        
        <div class="sec_1 page3">
            @include('includes.simple_menu')
            
           
        </div>
        <div style=" background-color: #F8F8F8;">
        <div id="crums" class="container" style="margin-bottom: 30px; max-width:1122px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div style="display: flex; flex-direction: row; flex-wrap: nowrap; justify-content: space-between; align-items: center;" class="info">
                <label style="margin-bottom:0; color: #000; padding: 10px 0">Complete Booking</label>
                <div class="book-left">
                    <a href="">Home</a><i style="color: #000;" class="fa fa-angle-right"></i><a href="">Booking</a>
                </div>
            </div>
        </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row offset-md-4 offset-lg-4">
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="card mb-3">
                    <h3 class="card_heading">Booking Summary</h3>
                    <hr></hr>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">Event Name</h4>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">{{$ed->event->title}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">Ticket Name</h4>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">{{$ed->ticket_name}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">Ticket Price</h4>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">{{$ed->ticket_price}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">Ticket Quantity</h4>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4 class="summary_heading">{{$ed->ticket_quantity}}</h4>
                        </div>
                    </div>

                    <form method="POST" action="{{url('/checkout')}}">
                        @csrf
                    <button class="register_button" type="submit">Proceed</button>
                    </form>
                </div>
            </div>
        </div>
        
        <!--sec 6 slider-->
        @include('includes.footer')
        
    </body>
    
</html>