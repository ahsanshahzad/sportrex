@extends('layout.main')
@section('links')
    <link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
    <link rel="stylesheet" href="https://tickethub.ng/assets/front/css/style.css">
@endsection
@section('content')
@include('includes.top_navbar')
<div class="bg-color"">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="home-title">
                    <h6>Home / Find Event</h6>
                    <h3 class = "text-black"></h3>
    
                    
                    <h4 class = "text-black">Serch for event.</h4>
                    <!-- <hr style="color: #fff;"> -->
                    <form method="post" action="{{route('search-events')}}">
                        @csrf
                    <div class="row col-12">
                    <input type="text" name="title" class="col-6" style="border: 0px;border-bottom: 1px solid darkgray;">
                    <input type="submit" name="" class="col-2 no-border" value="Search" style="border-radius: 0px;">
                </div>
                    <div class="row col-12">
                        <select class="text-black col-3 no-border" id="myDropdown" name="address" placeholder="Location">
                            <option selected>Location</option>
                            <option value="George Town">George Town</option>
                            <option value="Kuala Lumpur">Kuala Lumpur</option>
                            <option value="Seberang Perai">Seberang Perai</option>
                            <option value="Johor Bahru">Johor Bahru</option>
                            <option value="Ipoh">Ipoh</option>
                            <option value="Seremban">Seremban</option>
                            <option value="Malacca">Malacca</option>
                            <option value="Kota Kinabalu">Kota Kinabalu</option>
                            <option value="Kuantan">Kuantan</option>
                            <option value="Alor Setar">Alor Setar</option>
                            <option value="Kuala Terengganu">Kuala Terengganu</option>
                            <option value="Kuching">Kuching</option>
                            <option value="Miri">Miri</option>
                            <option value="Subang Jaya">Subang Jaya</option>
                            <option value="Petaling Jaya">Petaling Jaya</option>
                            <option value="Shah Alam">Shah Alam</option>
                            <option value="Pasir Gudang">Pasir Gudang</option>
                            <option value="Iskandar Puteri">Iskandar Puteri</option>
                        </select>
                        <div class="col-2"></div>
                        <select class="text-black col-3 no-border" name="time">
                            <option>All time</option>
                            <option value="00:00">00:00</option>
                            <option value="01:00">01:00</option>
                            <option value="02:00">02:00</option>
                            <option value="03:00">03:00</option>
                            <option value="04:00">04:00</option>
                            <option value="05:00">05:00</option>
                            <option value="06:00">06:00</option>
                            <option value="07:00">07:00</option>
                            <option value="08:00">08:00</option>
                            <option value="09:00">09:00</option>
                            <option value="10:00">10:00</option>
                            <option value="11:00">11:00</option>
                            <option value="12:00">12:00</option>
                            <option value="13:00">13:00</option>
                            <option value="14:00">14:00</option>
                            <option value="15:00">15:00</option>
                            <option value="16:00">16:00</option>
                            <option value="17:00">17:00</option>
                            <option value="18:00">18:00</option>
                            <option value="19:00">19:00</option>
                            <option value="20:00">20:00</option>
                            <option value="21:00">21:00</option>
                            <option value="22:00">22:00</option>
                            <option value="23:00">23:00</option>
                        </select>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
<div class="row">
<!-- Listing Item -->
@forelse($events as $event)
<div class="col-lg-4 col-md-6 margin-top-10">
    <a href="{{url('event/detail/'.$event->id)}}" class="listing-item-container">
        <div class="listing-item">
            <!-- <div class="listing-badge now-open" style="background: #FF5A19">From: ₦55,000</div> -->
            <img src="{{asset('public/assets/images/events/'.$event->image)}}" class="img-fluid" alt="Public Speaking Coaching">
        </div>
        <div class="star-rating" style="height: 170px;">
            <span><i class="fa fa-map-marker"></i> {{$event->venue_address}}</span><br>
            <h4><strong>{{$event->title}}</strong></h4>
            <span><i class="fa fa-calendar-check-o"></i> Saturday, January 08</span>
            <span class="button popup-with-zoom-anim share-this" title="Share with friends" href="#small-dialog" data-url="https://tickethub.ng/public-speaking-coaching-39072998" data-title="Public Speaking Coaching"><i class="like-icon im im-icon-Sharethis"></i></span>
        </div>
    </a>
</div>
@empty
<h3>No Record Found</h3>
@endforelse

<!-- Listing Item / End -->

</div>
</div>
@include('includes.footer')
@endsection