<html>
    <head>
        <title>Sportrex</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
        <link rel="stylesheet" href="{{asset('public/assets/css/carousel.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
        <link rel="stylesheet" href="./lib/font-awesome/css/all.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}">
        <style type="text/css">
    .bg-color{
        background-color: #ff5900;
    }
</style>
        
    </head>
    <body>
        
        <div class="sec_1 page3 mb-0">
            @include('includes.simple_menu')
            
           
        </div>
        <div style=" background-color: #F8F8F8;">
        <div id="crums" class="container" style="margin-bottom: 30px; max-width:1122px !important;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div style="display: flex; flex-direction: row; flex-wrap: nowrap; justify-content: space-between; align-items: center;" class="info">
                <label style="margin-bottom:0; color: #000; padding: 10px 0">Complete Booking</label>
                <div class="book-left">
                    <a href="">Home</a><i style="color: #000;" class="fa fa-angle-right"></i><a href="">Booking</a>
                </div>
            </div>
        </div>
        </div>
        <!-- =============== Form ============= -->
        <div class="sec3">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="container">
                        <div class="row">
                            <div id="form1" style="padding: 0; margin-top: 20px;" class="card1">
                                <h3 class="card_heading">Purchaser's Details</h3>
                                <form method="POST" action="{{url('/booking-info')}}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$event_detail->id}}">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputEmail4">Full Name</label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                
                                                <input type="text" name="p_name" value="{{old('p_name')}}" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                            </div>
                                            @error('p_name')
                                                <div class="alert alert-danger" role="alert">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4">E-mail Address</label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                <div class="input-group-prepend">
                                                    <div style="background-color: #fff;" class="input-group-text"><i class="far fa-envelope"></i></div>
                                                </div>
                                                <input style="border-left: none" type="email" name="p_email" value="{{old('p_email')}}" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                            </div>
                                            @error('p_email')
                                                <div class="alert alert-danger" role="alert">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword4">Phone</label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                <div class="input-group-prepend">
                                                    <div style="background-color: #fff;" class="input-group-text"><i class="fa fa-phone-alt"></i></div>
                                                </div>
                                                <input style="border-left: none" type="text" name="p_phone" class="form-control" id="inlineFormInputGroupUsername2" value="{{old('p_phone')}}" placeholder="">
                                            </div>
                                            @error('p_phone')
                                                <div class="alert alert-danger" role="alert">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="gridCheck">
                                            <label class="form-check-label" for="gridCheck">
                                                Would you like to use the same information for all other tickets?
                                            </label>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div style="background-color: #3A52F8 ; border: 1px solid #3A52F8 ;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div style="display: flex; flex-direction: row; flex-wrap: nowrap; justify-content: space-between; align-items: center;" class="info">
                                        <label style="color: #fff; padding: 10px">Ticket Owner(s) information</label>
                                        <i style="color: #fff;" class="far fa-times-circle"></i>
                                    </div>
                                </div>
                                <div style="border: 1px solid gray; border-top: none;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="container">
                                        <div class="row">
                                            <div id="form1" style="padding: 0 10px; margin-top: 20px;" class="card1">
                                                <h3 class="card_heading">1. Guys Owner's Details</h3>
                                                <div class="form-row">
                                                    <label style="margin-left: 5px; margin-top: 15;" for="inputAddress">Full Name</label>
                                                    <input style="margin: 0 5px; margin-bottom: 10px;" type="text" name="o_name[]" class="form-control" placeholder="">
                                                    
                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">E-mail Address</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="far fa-envelope"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_email[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Phone</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="fa fa-phone-alt"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_phone[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                                        <label class="form-check-label" for="gridCheck">
                                                            Would you like to use the same information for all other tickets?
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div id="form1" style="padding: 0 10px; margin-top: 20px;" class="card1">
                                                <h3 class="card_heading">2. Guys Owner's Details</h3>
                                                <div class="form-row">
                                                    <label style="margin-left: 5px; margin-top: 15;" for="inputAddress">Full Name</label>
                                                    <input style="margin: 0 5px; margin-bottom: 10px;" type="text" name="o_name[]" class="form-control" placeholder="">
                                                    
                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">E-mail Address</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="far fa-envelope"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_email[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Phone</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="fa fa-phone-alt"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_phone[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                                        <label class="form-check-label" for="gridCheck">
                                                            Would you like to use the same information for all other tickets?
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div id="form1" style="padding: 0 10px; margin-top: 20px;" class="card1">
                                                <h3 class="card_heading">3. Guys Owner's Details</h3>
                                                <div class="form-row">
                                                    <label style="margin-left: 5px; margin-top: 15;" for="inputAddress">Full Name</label>
                                                    <input style="margin: 0 5px; margin-bottom: 10px;" type="text" name="o_name[]" class="form-control" placeholder="">

                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">E-mail Address</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="far fa-envelope"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_email[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Phone</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="fa fa-phone-alt"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_phone[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                                        <label class="form-check-label" for="gridCheck">
                                                            Would you like to use the same information for all other tickets?
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div id="form1" style="padding: 0 10px; margin-top: 20px;" class="card1">
                                                <h3 class="card_heading">4. Ladies Owner's Details</h3>
                                                <div class="form-row">
                                                    <label style="margin-left: 5px; margin-top: 15;" for="inputAddress">Full Name</label>
                                                    <input style="margin: 0 5px; margin-bottom: 10px;" type="text" name="o_phone[]" class="form-control" placeholder="">

                                                    <div class="form-group col-md-6">
                                                        <label for="inputEmail4">E-mail Address</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="far fa-envelope"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_email[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Phone</label>
                                                        <div class="input-group mb-2 mr-sm-2">
                                                            <div class="input-group-prepend">
                                                                <div style="background-color: #fff;" class="input-group-text"><i class="fa fa-phone-alt"></i></div>
                                                            </div>
                                                            <input style="border-left: none" type="text" name="o_phone[]" class="form-control" id="inlineFormInputGroupUsername2" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="gridCheck">
                                                        <label class="form-check-label" for="gridCheck">
                                                            Would you like to use the same information for all other tickets?
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin: 30px 0; border: 1px solid gray;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="container">
                                        <div class="row">
                                            <div id="form1" style="margin-top: 20px;" class="card1">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                                    <label class="form-check-label" for="exampleRadios1">
                                                        Got any Coupon?
                                                    </label>
                                                </div>
                                                <div style="margin-top: 15px;" class="form-inline">
                                                    <div class="form-group mx-sm-3 mb-2" style="margin-left: 0.1rem!important;">
                                                        <!-- <label for="inputPassword2" class="sr-only">Enter Coupon Code</label> -->
                                                        <input type="password" class="form-control" id="inputPassword2" placeholder="Enter Coupon Code">
                                                    </div>
                                                    <button id="coupon1" style="border-color:#3A52F8 !important; background:#3A52F8 !important;"  type="submit" name="coupon" class="btn btn-primary mb-2">Verify coupon</button>
                                                </div>
                                                 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--sec 4 Get ticket-->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!--Follow Section-->
                                <div class="row organize_margin mt-0">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 button_margin text-right mb-3">
                                        <button id="bookingBtn" style="border-color:#3A52F8 !important; background:#3A52F8 !important;" type="submit" class = "follow_button" href="">Complete Booking</button> 
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                        <p class="">By clicking on complete booking you agree with the terms and conditions</p>

                                    </div>
                                    
                                   
                                </div>
                                
                            </div>
                            </form>
                            <!--sec 5 map-->
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="card">
                        <h3 class="card_heading">How to start an E-commerce
                        Business</h3>
                        <h5 class="card_sub_heading">Saturday, 25 September 2021, 8:00 AM</h5>
                        <p class="card_paragraph">Block A, 36545, Jalan TS 6/1, Taman
                            Perindustrian Subang, 47510 Subang Jaya,
                        Selangor</p>
                        <hr></hr>
                        <h3 class="card_heading">Booking Summary</h3>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h4 class="summary_heading">3x Normal</h4>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <h4 class="summary_heading">RM 120</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h4 class="summary_heading">1x Advance</h4>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <h4 class="summary_heading">RM 250</h4>
                            </div>
                        </div>
                        <hr></hr>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h4 class="card_heading">Total</h4>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <h4 class="card_heading_1">RM 350</h4>
                            </div>
                        </div>
                        <a class="register_button bg-color" href="">Booking</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- ========x======= Form =======x====== -->
        
        <br><br><br><br>
        
        <!--sec 6 slider-->
        @include('includes.footer')

        
    </body>
    <script src="assets/js/carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
    <!--counter 1-->
    <script>
    var count = 0;
    var countEl = document.getElementById("count");
    function plus() {
    count++;
    countEl.value = count;
    }
    function minus() {
    count--;
    countEl.value = count;
    }
    var count2 = 0;
    var countE2 = document.getElementById("count2");
    function plus2() {
    count2++;
    countE2.value = count2;
    }
    function minus2() {
    count2--;
    countE2.value = count2;
    }
    </script>
    <!--counter 2-->
    <script>
    //Slider 1
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider').length)
    {
    var viewedSlider = $('.bbb_viewed_slider');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    991:{items:4},
    1199:{items:6}
    }
    });
    if($('.bbb_viewed_prev').length)
    {
    var prev = $('.bbb_viewed_prev');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next').length)
    {
    var next = $('.bbb_viewed_next');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });
    }
    }
    });
    //Slider 2
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider_1').length)
    {
    var viewedSlider = $('.bbb_viewed_slider_1');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    }
    });
    if($('.bbb_viewed_prev_1').length)
    {
    var prev = $('.bbb_viewed_prev_1');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next_1').length)
    {
    var next = $('.bbb_viewed_next_1');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });}}
    });
    //Slider 3
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider_2').length)
    {
    var viewedSlider = $('.bbb_viewed_slider_2');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    }
    });
    if($('.bbb_viewed_prev_2').length)
    {
    var prev = $('.bbb_viewed_prev_2');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next_2').length)
    {
    var next = $('.bbb_viewed_next_2');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });}}
    });
    </script>
    <script>
    let map;
    function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 8,
    });
    }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg&callback=initMap&v=weekly&channel=2" async></script>
</html>