<html>
    <head>
        <title>Sportrex</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
        <link rel="stylesheet" href="{{asset('public/assets/css/carousel.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
        <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
        <link rel="stylesheet" href="./lib/font-awesome/css/all.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}">
        <style type="text/css">
    .bg-color{
        background-color: #ff5900;
    }
</style>
        
    </head>
    <body>
        
        <div class="sec_1 page3 mb-0">
            @include('includes.simple_menu')
            
           
        </div>
        <div class="sec4 page5_sec4 mt-0">

            

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row organize_margin mt-3">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <img id="pi" src="{{asset('public/assets/images/plus.png')}}" alt="">
                <h3 class="organize_heading">Sportizza Subang Sdn Bhd</h3>
                <h6 class="organize_heading_2">Experience & professional in multi sports business management that aiming to expand globally for the <br>development of younger generation in Malaysia</h6>
            </div> 
            </div><div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 button_margin">
                <a id="btnF" class = "follow_button" href="">Follow</a>
                    
                
                </div>
                    
                    </div> 
                </div>
    
        </div>
    </div>
</div>

<!--sec 3 Tabs-->

<div class="sec3">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <!-- Tabs -->

        <div class="container px-0">
            <div class="roww">
                    <!-- Nav tabs -->
                    <div id="cards1" style="margin-top: 60px;" class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#home" class="active" aria-controls="home" role="tab" data-toggle="tab">Upcomming</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Past</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div style="width: 100%;" class="card">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="card_heading">How to start an E-commerce Business</h3>
                                    <h5 class="card_sub_heading">Saturday, 25 September 2021, 8:00 AM</h5>
                                    <p class="card_paragraph">Block A, 36545, Jalan TS 6/1, Taman
                                        Perindustrian Subang, 47510 Subang Jaya,
                                        Selangor</p>
                                        <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-5">
                                            <h4 class="card_heading">RM100 </h4>
                                        </div>
                                    </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="bg-s">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; margin-top: 20px;" class="card">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="card_heading">How to start an E-commerce Business</h3>
                                    <h5 class="card_sub_heading">Saturday, 25 September 2021, 8:00 AM</h5>
                                    <p class="card_paragraph">Block A, 36545, Jalan TS 6/1, Taman
                                        Perindustrian Subang, 47510 Subang Jaya,
                                        Selangor</p>
                                        <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-5">
                                            <h4 class="card_heading">RM100 </h4>
                                        </div>
                                    </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="bg-s">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            <div role="tabpanel" class="tab-pane mb-3" id="profile">
                                <legend>Profile</legend>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                                <legend>Messages</legend>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                            <div role="tabpanel" class="tab-pane" id="settings">
                                <legend>Settings</legend>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passage..</div>
                          
                        </div>
                    </div>
            </div>
        </div>

<!-- ./Tabs -->
</div>
</div>
</div>
        
        <br><br><br><br>
        
        <!--sec 6 slider-->
        @include('includes.footer')

        
    </body>
    <script src="assets/js/carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
    <!--counter 1-->
    <script>
    var count = 0;
    var countEl = document.getElementById("count");
    function plus() {
    count++;
    countEl.value = count;
    }
    function minus() {
    count--;
    countEl.value = count;
    }
    var count2 = 0;
    var countE2 = document.getElementById("count2");
    function plus2() {
    count2++;
    countE2.value = count2;
    }
    function minus2() {
    count2--;
    countE2.value = count2;
    }
    </script>
    <!--counter 2-->
    <script>
    //Slider 1
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider').length)
    {
    var viewedSlider = $('.bbb_viewed_slider');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    991:{items:4},
    1199:{items:6}
    }
    });
    if($('.bbb_viewed_prev').length)
    {
    var prev = $('.bbb_viewed_prev');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next').length)
    {
    var next = $('.bbb_viewed_next');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });
    }
    }
    });
    //Slider 2
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider_1').length)
    {
    var viewedSlider = $('.bbb_viewed_slider_1');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    }
    });
    if($('.bbb_viewed_prev_1').length)
    {
    var prev = $('.bbb_viewed_prev_1');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next_1').length)
    {
    var next = $('.bbb_viewed_next_1');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });}}
    });
    //Slider 3
    $(document).ready(function()
    {
    if($('.bbb_viewed_slider_2').length)
    {
    var viewedSlider = $('.bbb_viewed_slider_2');
    viewedSlider.owlCarousel(
    {
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:5000,
    nav:false,
    dots:false,
    responsive:
    {
    0:{items:1},
    575:{items:2},
    768:{items:3},
    }
    });
    if($('.bbb_viewed_prev_2').length)
    {
    var prev = $('.bbb_viewed_prev_2');
    prev.on('click', function()
    {
    viewedSlider.trigger('prev.owl.carousel');
    });
    }
    if($('.bbb_viewed_next_2').length)
    {
    var next = $('.bbb_viewed_next_2');
    next.on('click', function()
    {
    viewedSlider.trigger('next.owl.carousel');
    });}}
    });
    </script>
    <script>
    let map;
    function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 8,
    });
    }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg&callback=initMap&v=weekly&channel=2" async></script>
</html>