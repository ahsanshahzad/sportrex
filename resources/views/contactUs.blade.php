<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900&display=swap" rel="stylesheet">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
		<link rel="stylesheet" href="{{asset('public/assets/css/carousel.css')}}">
		<link rel="stylesheet" href="{{asset('public/assets/css/navbar_black.css')}}">
		<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
		<link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.css')}}">
		
		<!-- Style -->
		<link rel="stylesheet" href="{{asset('public/assets/css/contct.css')}}">
		<title>Sportrex</title>
	</head>
	<body>
		<div class="sec_1 page2">
			@include('includes.simple_menu')
			
		</div>
		
		<div class="content spaceC">
			
			<div class="container">
				<div class="row">
					<div class="col-md-5 mr-auto">
						<h2 class="c-heading">Contact Us</h2>
						<p class="mb-md-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste quaerat autem corrupti asperiores accusantium et fuga! Facere excepturi, quo eos, nobis doloremque dolor labore expedita illum iusto, aut repellat fuga!</p>
						<ul class="  mb-md-5 my-list">
							<li class="d-flex text-black mb-2">
								<span ><span class="icon-map"></span></span> 34 Street Name, City Name Here, <br> United States
							</li>
							<li class="d-flex text-black mb-2"><span ><span class="icon-phone"></span></span> +1 (222) 345 6789</li>
							<li class="d-flex text-black"><span ><span class="icon-envelope-o"></span></span> info@mywebsite.com </li>
						</ul>
					</div>
					<div class="col-md-6">
						@include('layout.partials.errors')
						<form class="mb-5" method="post" action="{!! route('contactus') !!}" id="contactForm" name="contactForm">
			                @csrf
			                @method('POST')
							<div class="row">
								
								<div class="col-md-12 form-group">
									<label for="name" class="col-form-label">Name</label>
									<input type="text" class="form-control" name="name" id="name">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 form-group">
									<label for="email" class="col-form-label">Email</label>
									<input type="text" class="form-control" name="email" id="email">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 form-group">
									<label for="message" class="col-form-label">Message</label>
									<textarea class="form-control" name="message" id="message" cols="30" rows="7"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Send Message" class="btn btn-primary rounded-0 py-2 px-4">
									<span class="submitting"></span>
								</div>
							</div>
						</form>
						<div id="form-message-warning mt-4"></div>
						<div id="form-message-success">
							Your message was sent, thank you!
						</div>
					</div>
				</div>
			</div>
			@include('includes.footer')
		</body>
	</html>