<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CouponController;



// Route::get('/', function () {
//     return view('welcome');
// });
// Auth::routes();
Route::get('/',[HomeController::class, 'index']);
Route::get('find-event',[HomeController::class, 'findEvent'])->name('findEvent');
Route::get('/login',[AuthController::class, 'showLogin'])->name('login');
Route::post('/login',[AuthController::class, 'login']);
Route::get('/register',[AuthController::class, 'showRegistration'])->name('register');
Route::post('/register',[AuthController::class, 'register']);
Route::post('/logout',[AuthController::class, 'logout'])->name('logout');
Route::view('/contact','contactUs')->name('contact');
Route::post('/contact-us',[HomeController::class, 'contacts'])->name('contactus');

Route::get('/category/view/{id}',[HomeController::class, 'cat_view'])->name('event-category');
Route::get('/event/detail/{id}',[HomeController::class, 'event_detail']);
Route::get('/booking/{id}',[HomeController::class, 'booking']);
Route::post('/booking-info',[HomeController::class, 'booking_info']);
Route::get('/checkout/{id}',[HomeController::class, 'checkout']);
Route::post('/checkout',[HomeController::class, 'checkout_info']);
Route::post('/search/events',[HomeController::class, 'catSearchEvents'])->name('cat-search-events');
Route::post('/all/search/events',[HomeController::class, 'allSearchEvents'])->name('search-events');
Route::get('/events/search',[HomeController::class, 'indexEventSearch'])->name('index.search');
Route::get('/orgnizer-details',[HomeController::class, 'test']);

Route::group(['middleware' => 'isLogin'], function(){
	Route::view('/dashboard','user.dashboard');
	
	// Event Routes
	// Route::get('/events',[EventController::class,'index'])->name('events');
	Route::resource('events', EventController::class);
	Route::view('event/create','user.events.create_new_event');
	Route::post('/store/event',[EventController::class,'store'])->name('create.event');
	// Sales Routes
	Route::get('sales/reports',[EventController::class,'sale'])->name('sales.report');
	Route::get('tickets/reports',[EventController::class,'ticket'])->name('ticket.report');

	Route::resource('coupons', CouponController::class);
	// Route::post('coupon/store',[CouponController::class,'store'])->name('coupons.store');
	
});
